package aventus.apiMd.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;

public class Response {
    HttpResponse httpResponse;
    public JSONObject jsonObject;

    public Response(HttpResponse httpResponse) {
        this.httpResponse = httpResponse;
        try {
            jsonObject = new JSONObject(getBody());
        } catch (org.json.JSONException e) {
            //.
        }

        System.out.println("Response: ");
        System.out.println(jsonObject);
    }

    public int getCode() {
        return httpResponse.getStatusLine().getStatusCode();
    }

    public String getBody() {
        HttpEntity entity = httpResponse.getEntity();
        String body = "";
        try {
            body = EntityUtils.toString(entity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return body;
    }
}
