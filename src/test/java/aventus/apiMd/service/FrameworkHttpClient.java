package aventus.agis2.API_center;

import aventus.apiMd.service.Response;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import java.io.IOException;

public class FrameworkHttpClient {
    protected HttpClient httpClient;

    public FrameworkHttpClient() {
        httpClient = HttpClientBuilder.create().build();
    }

    public Response get(String url)
    {
        HttpGet httpGet = new HttpGet(url);

        try {
            return new Response(httpClient.execute(httpGet));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Response post(String url, JSONObject body, String apiKey)
    {
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("content-type", "application/json");
        httpPost.addHeader("apikey", apiKey);
        try {
            httpPost.setEntity(new StringEntity(body.toString()));
            return new Response(httpClient.execute(httpPost));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Response patch(String url, String apiKey, JSONObject body)
    {
        HttpPatch httpPatch = new HttpPatch(url);
        httpPatch.addHeader("content-type", "application/json");
        httpPatch.addHeader("apikey", apiKey);

        try {
            httpPatch.setEntity(new StringEntity(body.toString()));
            return new Response(httpClient.execute(httpPatch));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Response put(String url, JSONObject body)
    {
        HttpPut httpPut = new HttpPut(url);

        try {
            httpPut.setEntity(new StringEntity(body.toString()));
            return new Response(httpClient.execute(httpPut));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Response delete(String url)
    {
        HttpDelete httpDelete = new HttpDelete(url);

        try {
            return new Response(httpClient.execute(httpDelete));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
