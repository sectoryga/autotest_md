package aventus.apiMd;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import okhttp3.*;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import userdata.UserRegistrationData;

import java.io.File;
import java.io.IOException;

public class RegistrationApi {
    UserRegistrationData userData = new UserRegistrationData();
    public int SmsCode;
    public int UserId;
    public int LoanId;
    public String UserToken;

    @Test
    public void UserRegistration() {
        System.out.println("Start test CreateNewUserWithLoanRequest");
        UserRegistrationData userData = new UserRegistrationData();
        RestAssured.baseURI = "https://api.master.md.aventus.work";
        RequestSpecification httpRequest = RestAssured.given();
        JSONObject requestParams = new JSONObject();
        requestParams.put("first_name", "Java" + (int) Math.random());
        requestParams.put("last_name", "Autotest");
        requestParams.put("mobile_phone", "373" + userData.getUserRandomTelephoneNumber());
        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toString());
        Response response = httpRequest.request(Method.POST, "/user");
        String responseBody = response.body().asString();
        JSONObject userResponse = new JSONObject(responseBody);
        SmsCode = userResponse.getInt("code");
        UserId = userResponse.getInt("id");
        UserToken = userResponse.getString("token");
        Assert.assertEquals(201, response.getStatusCode());
        System.out.println("UserRegistration completed");
    }

    @Test(dependsOnMethods = {"UserRegistration"})
    public void PhoneConfirmation() {
        RestAssured.baseURI = "https://api.master.md.aventus.work/user/" + UserId + "/phone-confirmation";
        RequestSpecification httpRequest = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("code", SmsCode);
        httpRequest.header("Content-Type", "application/json");
        httpRequest.header("apikey", UserToken);
        httpRequest.body(requestParams.toString());
        Response response = httpRequest.request(Method.POST);
        Assert.assertEquals(response.getStatusCode(), 200);
        System.out.println("PhoneConfirmation completed");
    }

    @Test(dependsOnMethods = {"PhoneConfirmation"})
    public void FileUpload() throws IOException {
        OkHttpClient client = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("files", "photoApi.png",
                        RequestBody.create(MediaType.get("image/png"), new File("./src/photoApi.png")))
                .build();
        Request request = new Request.Builder()
                .url("https://api.master.md.aventus.work/user/file-upload")
                .addHeader("apikey", UserToken)
                .post(requestBody)
                .build();
        okhttp3.Response response = client.newCall(request).execute();
        Assert.assertEquals(response.code(), 200);
        System.out.println("FileUpload completed");
    }

    @Test(dependsOnMethods = {"FileUpload"})
    public void UpdateUserInformation() {
        RestAssured.baseURI = "https://api.master.md.aventus.work/user/" + UserId;
        RequestSpecification httpRequest = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("personal_id", "09925155" + (11111 + (int) (Math.random() * 89999)));
        requestParams.put("document_number", "A155" + (11111 + (int) (Math.random() * 89999)));
        requestParams.put("gender", "m");
        requestParams.put("date_of_birth", "1982-08-16");
        requestParams.put("email", "java@test.com");
        requestParams.put("monthly_income", "123456");
        requestParams.put("marital_status", 2);
        requestParams.put("number_of_children", 4);
        requestParams.put("bank_account_number", "MD24AG000225100013104168");
        requestParams.put("requested_amount", 5000);
        requestParams.put("requested_days", 10);
        requestParams.put("method_of_disbursement", 1);
        httpRequest.body(requestParams.toString());

        httpRequest.header("Content-Type", "application/json");
        httpRequest.header("apikey", UserToken);

        Response response = httpRequest.request(Method.PATCH);

        Assert.assertEquals(response.getStatusCode(), 200);
        System.out.println("UpdateUserInformation completed");
    }

    @Test(dependsOnMethods = {"UpdateUserInformation"})
    public void LoanRequest() {
        RestAssured.baseURI = "https://api.master.md.aventus.work/loan";
        RequestSpecification httpRequest = RestAssured.given();

        JSONObject requestParams = new JSONObject();
        requestParams.put("user_id", UserId);
        requestParams.put("loan_sum", 5000);
        requestParams.put("loan_period", 10);
        requestParams.put("product_id", 3);
        requestParams.put("target_url", "https://test.md/?utm_source=java");
        httpRequest.body(requestParams.toString());
        httpRequest.header("Content-Type", "application/json");
        httpRequest.header("apikey", UserToken);
        Response response = httpRequest.request(Method.POST);
        String responseBody = response.body().asString();
        JSONObject loanResponse = new JSONObject(responseBody);
        LoanId = loanResponse.getInt("id");
        System.out.println("LoanRequest sent successfully");
        Assert.assertEquals(response.getStatusCode(), 200);
    }
}
