package aventus;

import aventus.agis2.pages.AbstractPage;
import aventus.webMd.helpers.frontRegHelpers.*;
import org.testng.annotations.Test;

import java.awt.*;

public class FrontRegistration extends AbstractPage {

    RegistrationStep1Helper registrationStep1Credit7Helper = new RegistrationStep1Helper();
    RegistrationStep2Helper registrationStep2Credit7Helper = new RegistrationStep2Helper();
    RegistrationStep3Helper registrationStep3Credit7Helper = new RegistrationStep3Helper();
    RegistrationStep4Helper registrationStep4Credit7Helper = new RegistrationStep4Helper();
    RegistrationStep5Helper registrationStep5Credit7Helper = new RegistrationStep5Helper();
    RegistrationStep6Helper registrationStep6Credit7Helper = new RegistrationStep6Helper();

    @Test(skipFailedInvocations = true)
    public void Credit7_RegistrationStep1_Test() {
        System.out.println("Test 1 - 1");
        registrationStep1Credit7Helper
                .getCredit7();
    }

    @Test(skipFailedInvocations = true)
    public void Credit7_RegistrationStep2_Test() throws InterruptedException {
        System.out.println("Test 1 - 2");
        registrationStep2Credit7Helper
                .getCredit7_2();
    }

    @Test(skipFailedInvocations = true)
    public void Credit7_RegistrationStep3_Test() {
        System.out.println("Test 1 - 3");
        registrationStep3Credit7Helper
                .getCredit7_3();
    }

    @Test(skipFailedInvocations = true)
    public void Credit7_RegistrationStep4_Test() {
        System.out.println("Test 1 - 4");
        registrationStep4Credit7Helper.getCredit7_Step4();
    }

    @Test(skipFailedInvocations = true)
    public void Credit7_RegistrationStep5_Test() throws InterruptedException {
        System.out.println("Test 1 - 5");
        registrationStep5Credit7Helper.getCredit7_Step5();
    }

    @Test(skipFailedInvocations = true)
    public void Credit7_RegistrationStep6_Test() throws AWTException, InterruptedException {
        System.out.println("Test 1 - 6");
        registrationStep6Credit7Helper.getCredit7_Step6();
    }

}
