package aventus.agis2.pages;

import aventus.agis2.Generators.*;

import java.io.File;

public class Page_300_Credit365_1stCredit_Registration extends AbstractPage {

    String setPriceField = "//input[@class='calculate__price-input']";
    String getMoneyButton = "//a[@href='/user/ru/application/registration']";
    // STEP_2
    String userSurnameField = "//*[@data-id='last_name']";
    String userNameField = "//*[@data-id='first_name']";
    String userPhoneField = "//*[@data-id='mobile_phone']";
    String userPasswordField = "//*[@data-id='password']";
    String checkBox1 = "//form-control[5]//div[1]//form-control-checkbox[1]//div[1]//label[1]//span[1]";
    String checkBox2 = "//form-control[6]//div[1]//form-control-checkbox[1]//div[1]//label[1]//span[1]";
    String ContinueButtonField = "Продолжить";
    // STEP_3
    String userIDNPField = "//*[@data-id='personal_id']";
    String userDateOfBirthfield = "//*[@data-id='date_of_birth']";
    String userGenderField = "app-checkbox-label";
    String userEmailField = "//*[@data-id='email']";
    String userIBAN = "//*[@data-id='btn-bank_account_number']";
    public String idnp;
    // STEP_4
    String dropDownMaritalStatusField = "//*[@data-id='marital_status']";
    String dropDownEducationField = "//*[@data-id='education']";
    String dropDownActivityField = "//*[@data-id='activity_type']";
    String organizationNameField = "//*[@data-id='organization_name']";
    String dropDownMonthlyIncomeField = "//*[@data-id='monthly_income']";
    String dropDownSelectorFirstElement = ".ng-option.ng-star-inserted.ng-option-marked";
    // STEP_5
    public String uploadDocument1Field = "//*[@data-id='upload-front']";
    public String uploadDocument2Field = "//*[@data-id='upload-back']";
    public File userDocument_2 = new File("./src/photoTwo.jpg");
    public File userDocument_1 = new File("./src/photoOne.jpg");
    // STEP_6
    String smsClientConfirmationAgis2Field = "mark";
    String smsConfirmationField = "//*[@data-id='code']";
    String getSendLoanButtonField = "//*[@data-id='btn-continue']";
    String messageField = "messageBlock";
    String checkLoanButtonField = ".pa-nav__link";
    private String sms;


    public String  ContinueButtonField_Step2345 = "/html/body/app-root/app-global-wrapper/app-application-wrapper/main/section/div/div/app-personal-information-page/div/app-personal-information-form/form/div[5]/form-control/div/form-button/button/span";


    public Page_300_Credit365_1stCredit_Registration openMainPageCredit365() {
        openUrl(Credit365_URL);
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setPriceCredit365() {
        getElement(setPriceField).sendKeys("\b\b\b\b\b\b");
        getElement(setPriceField).sendKeys("12000");
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration clickGetMoneyButton() {
        waitForElementVisible(getElementBy(getMoneyButton));
        waitForElementClickable(getElementBy(getMoneyButton));
        getElement(getMoneyButton).click();
        System.out.println("Sum = 20000");
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserSurname() throws InterruptedException {
        Thread.sleep(1000);
        waitForElementVisible(getElementBy(userSurnameField));
        getElement(userSurnameField).sendKeys(Surname_random_generator.getSurname());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserName() {
        waitForElementVisible(getElementBy(userNameField));
        getElement(userNameField).sendKeys(Name_random_generator.getName());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserPhone() {
        waitForElementVisible(getElementBy(userPhoneField));
        String userPhone = Phone_number_random_generator.getPhone();
        getElement(userPhoneField).sendKeys(userPhone);
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserPassword() {
        waitForElementVisible(getElementBy(userPasswordField));
        getElement(userPasswordField).sendKeys(Password_random_generator.getPassword());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setConsentCheckBox1() {
        waitForElementVisible(getElementBy(checkBox1));
        getElement(checkBox1).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setConsentCheckBox2() {
        waitForElementVisible(getElementBy(checkBox2));
        getElement(checkBox2).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration clickContinueButton() {
        waitForElementVisible(getElementByText(ContinueButtonField));
        getElementText(ContinueButtonField).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserIDNP() {
        waitForElementVisible(getElementBy(userIDNPField));
        String idnp = IDNP_random_generator.getUserIDNP();
        getElement(userIDNPField).sendKeys(idnp);
        this.idnp = idnp;
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserDateOfBirth() {
        waitForElementVisible(getElementBy(userDateOfBirthfield));
        getElement(userDateOfBirthfield).sendKeys(DateOfBirth_random_generator.getUserDateOfBirth());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserGender() {
        waitForElementVisible(getElementByClass(userGenderField));
        getElementC(userGenderField).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserEmail() {
        waitForElementVisible(getElementBy(userEmailField));
        getElement(userEmailField).sendKeys(Email_random_generator.getUserEmail());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserIBAN() {
        waitForElementVisible(getElementBy(userIBAN));
        getElement(userIBAN).sendKeys(IBAN_random_generator.getUserIBAN());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration waitDropDownSelectorFirstElementField() {
        waitForElementClickable(getElementByCss(dropDownSelectorFirstElement));
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration waitUserMaritalStatusField() {
        waitForElementClickable(getElementBy(dropDownMaritalStatusField));
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserMaritalStatus() {
        waitUserMaritalStatusField().getElement(dropDownMaritalStatusField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration waitUserEducationStatusField() {
        waitForElementClickable(getElementBy(dropDownEducationField));
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserEducationStatus() {
        waitUserEducationStatusField().getElement(dropDownEducationField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration waitUserActivityType() {
        waitForElementClickable(getElementBy(dropDownActivityField));
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserActivityType() {
        waitUserActivityType();
        getElement(dropDownActivityField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration waitUserOrganizationNameField() {
        waitForElementClickable(getElementBy(organizationNameField));
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserOrganizationName() {
        waitUserOrganizationNameField();
        getElement(organizationNameField).sendKeys("aventus");
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration waitUserMonthlyIncome() {
        waitForElementClickable(getElementBy(dropDownMonthlyIncomeField));
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserMonthlyIncome() {
        waitUserMonthlyIncome();
        getElement(dropDownMonthlyIncomeField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserDocument1() {
        waitForElementVisible(getElementBy(uploadDocument1Field));
        getElement(uploadDocument1Field)
                .sendKeys(userDocument_1.getAbsolutePath());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration setUserDocument2() throws InterruptedException {
        Thread.sleep(1000);
        waitForElementVisible(getElementBy(uploadDocument1Field));
        waitForElementVisible(getElementBy(uploadDocument2Field));
        getElement(uploadDocument2Field)
                .sendKeys(userDocument_2.getAbsolutePath());
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration clickContinueButton_Step2345(){
        waitForElementVisible(getElementBy(ContinueButtonField_Step2345));
        waitForElementClickable(getElementBy(ContinueButtonField_Step2345));
        getElement(ContinueButtonField_Step2345).click();
        return this;
    }

    public String getSMS_UserRegistrationConfirmation() {
        openUrl(AGIS2_allUsersURL);
        String sms = getElementTag(smsClientConfirmationAgis2Field).getText();
        System.out.println(sms);
        this.sms = sms;
        return sms;
    }

    public Page_300_Credit365_1stCredit_Registration pasteSMS() {
        getElement(smsConfirmationField).click();
        getElement(smsConfirmationField).sendKeys(sms);
        return this;
    }

    public Page_300_Credit365_1stCredit_Registration clickSendLoanButton() {
        waitForElementClickable(getElementBy(getSendLoanButtonField));
        getElement(getSendLoanButtonField).click();
        waitForElementVisible(getElementById(messageField));
        waitForElementClickable(getElementByCss(checkLoanButtonField));
        return this;
    }

}


