package aventus.agis2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.FrameworkCore;
import utils.PauseLenght;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AbstractPage extends FrameworkCore {
    protected static WebDriver driver = getInstance();
    private int index;

    public WebDriver getDriver() {
        return driver;
    }

    public void openUrl(String url) {
        driver.manage().window().maximize();
        driver.get(url);
    }

    public static void waitForElementVisible(By by) {
        try {
            WebDriverWait waiter = new WebDriverWait(driver, 3);
            waiter.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }
    }

    public static void waitForElementClickable(By by) {
        try {
            WebDriverWait waiter = new WebDriverWait(driver, PauseLenght.MAX.value());
            waiter.until(ExpectedConditions.elementToBeClickable(by));
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }
    }

    public static By getElementBy(String element) {
        return By.xpath(element);
    }

    public static By getElementById(String element) {
        return By.id(element);
    }

    public static By getElementByCss(String element) {
        return By.cssSelector(element);
    }

    public static By getElementByClass(String element) {
        return By.className(element);
    }

    public static By getElementByText(String element) {
        return By.linkText(element);
    }

    public static By getElementByTextPart(String element) {
        return new By.ByPartialLinkText(element);
    }

    public static By getElementByName(String element) {
        return By.name(element);
    }

    public static By getElementByTag(String element) {
        return By.tagName(element);
    }

    public WebElement getElement(String element) {
        return driver.findElement(By.xpath(element));
    }

    public WebElement getElementName(String element) {
        return driver.findElement(By.name(element));
    }

    public WebElement getElementTag(String element) {
        return driver.findElement(By.tagName(element));
    }

    public WebElement getElementCss(String element) {
        return driver.findElement(By.cssSelector(element));
    }

    public WebElement getElementC(String element) {
        return driver.findElement(By.className(element));
    }

    public WebElement getElementId(String element) {
        return driver.findElement(By.id(element));
    }

    public WebElement getElementText(String element) {
        return driver.findElement(By.linkText(element));
    }

    public WebElement getElementTextPart(String element) {
        return driver.findElement(new By.ByPartialLinkText(element));
    }


    public List<WebElement> getElements(String element) {
        return driver.findElements(By.xpath(element));
    }

    // ---------------------------------------------------------------------- //
    //Взять все вкладки
    public List<String> getTabs() {
        Set<String> wh = driver.getWindowHandles();
        List<String> list = new ArrayList<>(wh);
        return list;
    }

    public void navigateToTab(int num, List<String> webHendlers) {
        driver.switchTo().window(webHendlers.get(num));
    }

    public int getResultsNumber(String element) {
        String str = getElement(element).getText();
        String[] transfer = str.split(" ");
        return Integer.valueOf(transfer[0]);
    }

    public void jsExe() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        // js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("window.scrollBy(0,1700)", "");
    }

    public void openNewTabInChrome() throws AWTException {
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_T);
        robot.keyRelease(KeyEvent.VK_CONTROL);

        String parentHandle = driver.getWindowHandle(); // get the current window handle

        for (String winHandle : driver.getWindowHandles()) {
            System.out.println(winHandle);
            driver.switchTo().window(winHandle);
        }
    }

    public WebDriver newTabs(WebDriver driver) {
        try {
            List<String> windowHandles = new ArrayList<>(driver.getWindowHandles());
            return driver.switchTo().window(windowHandles.get(index));
        } catch (IndexOutOfBoundsException windowWithIndexNotFound) {
            return null;
        }
    }

    //_______________________________ ADMIN BUTTONS__________________________________//
    public String becomeVerifierField = ".btn.btn-primary"; //by id

    public AbstractPage clickBecomeVerifierButton() {
        waitForElementVisible(getElementByCss(becomeVerifierField));
        getElementCss(becomeVerifierField).click();
        return this;
    }

    public String decideButtonField = "transition-33"; //id

    public AbstractPage clickDecideButton() {
        waitForElementVisible(getElementById(decideButtonField));
        getElementId(decideButtonField).click();
        return this;
    }

    public String saveButtonAGIS2 = "btn_update_and_edit";

    public AbstractPage clickSaveButtonAGIS2() {
        waitForElementVisible(getElementBy(saveButtonAGIS2));
        getElementName(saveButtonAGIS2).click();
        return this;
    }

    public String decisionDropDownField = "decision_select"; //Id
    public String choseToApproveLoanField = "//*[@id='select2-chosen-20']";
    public String approveDropDownField = "//*[@id='select2-result-label-33']";

    public AbstractPage toApproveLoan() {
        waitForElementVisible(getElementById(decisionDropDownField));
        driver.findElement(By.xpath(choseToApproveLoanField)).click();
        driver.findElement(By.xpath(approveDropDownField)).click();
        return this;
    }

    //_______________________________________________________________________________//
    String AdminUserNameField = "username";
    String AdminUserPasswordField = "password";
    String submitAdminButton = "submit";

    public AbstractPage setAdminUserName() {
        getElementId(AdminUserNameField).sendKeys(adminUserName);
        return this;
    }

    public AbstractPage setAdminUserPassword() {
        waitForElementClickable(getElementById(AdminUserPasswordField));
        getElementId(AdminUserPasswordField).sendKeys(adminUserPassword);
        return this;
    }

    public AbstractPage clickLoginButtonAGIS2() {
        getElementId(submitAdminButton).click();
        return this;
    }
}
