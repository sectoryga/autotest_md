package aventus.agis2.pages;

public class Page_301_Credit365_1stCredit_Review extends AbstractPage {

    public String userApplicationField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[1]/div/a[3]";
    public String addressField = "/html/body/div[1]/div/section[2]/div[2]/form/div[1]/div/div/div[4]/div/div/div[2]/div/div/div/div/span[2]/a";
    public String dropDownAdressField = "/html/body/div[1]/div/section[2]/div[2]/form/div[1]/div/div/div[4]/div/div/div[2]/div/div/div/div/span[1]/table/tbody/tr/td[2]";
    public String choseRegionDropDownField = "//*[@id=\"select2-result-label-39\"]";
    public String ConfirmForCustomerButton = "//*[@id=\"transition-20\"]";

    public Page_301_Credit365_1stCredit_Review openAllApplicationsAndLoans() throws InterruptedException {
        openUrl(AGIS2_AllApplicationsAndLoans);
        Thread.sleep(1000);
        System.out.println("openAllApplicationsAndLoans is done");
        return this;
    }

    public Page_301_Credit365_1stCredit_Review openUserApplication() {
        waitForElementVisible(getElementBy(userApplicationField));
        getElement(userApplicationField).click();
        System.out.println("openUserApplication is done");
        return this;
    }

    public Page_301_Credit365_1stCredit_Review setUserAddress() throws InterruptedException {
        jsExe();
        Thread.sleep(1000);
        waitForElementVisible(getElementBy(addressField));
        waitForElementClickable(getElementBy(addressField));
        getElement(addressField).click();
        Thread.sleep(2000);
        waitForElementVisible(getElementBy(dropDownAdressField));
        waitForElementClickable(getElementBy(dropDownAdressField));
        getElement(dropDownAdressField).click();
        waitForElementVisible(getElementBy(choseRegionDropDownField));
        getElement(choseRegionDropDownField).click();
        System.out.println("setUserAddress is done");
        return this;
    }

    public Page_301_Credit365_1stCredit_Review loanApplicationConfirmationByAdminFromAdmin() {
        waitForElementClickable(getElementBy(ConfirmForCustomerButton));
        getElement(ConfirmForCustomerButton).click();
        return this;
    }


}
