package aventus.agis2.pages;

public class Page_157_Credit7_1stCredit_IssueAndRepaymentLoan_Step1 extends AbstractPage {

    public String giveOutLoanButtonField = "/html/body/div[1]/div/section[2]/div[2]/form/div[1]/div/div/div/div/div[2]/div/div[1]/div/div[1]/button[2]";
    public String completeTheIssuanceField = "//*[@id=\"transition-5\"]";


    public Page_157_Credit7_1stCredit_IssueAndRepaymentLoan_Step1 toGiveOutLoan() {
        waitForElementClickable(getElementBy(giveOutLoanButtonField));
        waitForElementVisible(getElementBy(giveOutLoanButtonField));
        getElement(giveOutLoanButtonField).click();
        return this;
    }

    public Page_157_Credit7_1stCredit_IssueAndRepaymentLoan_Step1 toCompleteTheIssuance() {
        waitForElementVisible(getElementBy(completeTheIssuanceField));
        waitForElementClickable(getElementBy(completeTheIssuanceField));
        getElement(completeTheIssuanceField).click();
        return this;
    }
}
