package aventus.agis2.pages;

import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_13_AGIS2_Displaying_Items_In_a_Application extends AbstractPage {

    String editButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[1]/a";
    String automaticDecisionButton_AdminField = "/html/body/div[1]/div/section[1]/div/nav/div/div/div/ul[1]/li[2]/a";
    String automaticDecision_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/h3";
    String smsButton_inApplication_AdminField = "/html/body/div[1]/div/section[1]/div/nav/div/div/div/ul[2]/li[1]/a";
    String sms_checkPage_inApplication_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[2]/div/div[2]/label";

    public Page_13_AGIS2_Displaying_Items_In_a_Application openEditPage_in_Application() {
        waitForElementVisible(getElementBy(editButton_AdminField));
        getElement(editButton_AdminField).click();
        waitForElementClickable(getElementBy(editButton_AdminField));
        WebElement element = getElement(editButton_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_13_AGIS2_Displaying_Items_In_a_Application openAutomatic_decision_in_Application() {
        waitForElementVisible(getElementBy(automaticDecisionButton_AdminField));
        getElement(automaticDecisionButton_AdminField).click();
        waitForElementClickable(getElementBy(automaticDecision_checkPage_AdminField));
        WebElement element = getElement(automaticDecision_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_13_AGIS2_Displaying_Items_In_a_Application openSMS_in_Application() {
        waitForElementVisible(getElementBy(smsButton_inApplication_AdminField));
        getElement(smsButton_inApplication_AdminField).click();
        waitForElementClickable(getElementBy(sms_checkPage_inApplication_AdminField));
        WebElement element = getElement(sms_checkPage_inApplication_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }


}
