package aventus.agis2.pages;

public class Page_158_Credit7_1stCredit_Repayment extends AbstractPage {

    public String incomeField = "/html/body/div[1]/div/section[1]/div/nav/div/div/div/ul[1]/li[4]/a";
    public String addNewIncomeField = "/html/body/div[1]/div/section[1]/div/nav/div/div/ul[1]/li/a";
    public String setIncomeAmountField = "input[id$=\"_income\"]";
    public String previewButtonField = "/html/body/div[1]/div/section[2]/div/form/div[2]/button";
    public String approveAndProceedToLoanEditingField = "/html/body/div[1]/div/section[2]/div[2]/div/form/button[1]";

    public Page_158_Credit7_1stCredit_Repayment clickIncomeButton() {
        waitForElementClickable(getElementBy(incomeField));
        waitForElementVisible(getElementBy(incomeField));
        getElement(incomeField).click();
        return this;
    }

    public Page_158_Credit7_1stCredit_Repayment clickAddNewIncome() {
        waitForElementClickable(getElementBy(addNewIncomeField));
        waitForElementVisible(getElementBy(addNewIncomeField));
        getElement(addNewIncomeField).click();
        return this;
    }

    public Page_158_Credit7_1stCredit_Repayment setIncomeAmount() {
        waitForElementClickable(getElementByCss(setIncomeAmountField));
        waitForElementVisible(getElementByCss(setIncomeAmountField));
        getElementCss(setIncomeAmountField).clear();
        getElementCss(setIncomeAmountField).sendKeys("7700");
        return this;
    }

    public Page_158_Credit7_1stCredit_Repayment clickPreviewButton() {
        waitForElementClickable(getElementBy(previewButtonField));
        waitForElementVisible(getElementBy(previewButtonField));
        getElement(previewButtonField).click();
        return this;
    }

    public Page_158_Credit7_1stCredit_Repayment clickApproveAndProceedToLoanEditing() {
        jsExe();
        waitForElementClickable(getElementBy(approveAndProceedToLoanEditingField));
        waitForElementVisible(getElementBy(approveAndProceedToLoanEditingField));
        getElement(approveAndProceedToLoanEditingField).click();
        return this;
    }
}
