package aventus.agis2.XML_operations;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class AbstractXML {

    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    public DocumentBuilder docBuilder;

    {
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

   Document doc = docBuilder.newDocument();

}
