
package aventus.agis2.Generators;

import java.util.HashMap;

public class IDNP_random_generator {
    private static HashMap<Integer, Integer> numbers = null;

    public static String getUserIDNP() {
        int FIRST_FACTOR = 7;
        int SECOND_FACTOR = 3;

        numbers = new HashMap<>();
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= 12; i++) {
            numbers.put(i, (int) (Math.random() * 9));
            result.append(numbers.get(i));
        }
        int number13 = (FIRST_FACTOR * sum(1, 4, 7, 10) + SECOND_FACTOR * sum(2, 5, 8, 11) + sum(3, 6, 9, 12)) % 10;
        result.append(number13);
        System.out.println("UserIDNP : " + result);
        return result.toString();
    }
    private static int sum(int ...indexes)
    {
        int sum = 0;
        for (int index: indexes) {
            sum += numbers.get(index);
        }
        return sum;
    }
}
