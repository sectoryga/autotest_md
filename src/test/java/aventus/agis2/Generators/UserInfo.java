package aventus.agis2.Generators;

public class UserInfo
{
    public static UserInfo instance = new UserInfo();

    public String phone_without_region = Phone_number_random_generator.getPhone();
    public String phone = "373" + phone_without_region;
    public String first_name = Name_random_generator.getName();
    public String last_name = Surname_random_generator.getSurname();
    public String password = Password_random_generator.getPassword();
    public String personal_id = IDNP_random_generator.getUserIDNP();
    public String date_of_birth = DateOfBirth_random_generator.getUserDateOfBirth();
    public String email = Email_random_generator.getUserEmail();
    public String IBAN = IBAN_random_generator.getUserIBAN();
}
