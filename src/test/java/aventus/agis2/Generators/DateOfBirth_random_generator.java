package aventus.agis2.Generators;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateOfBirth_random_generator {
    public static String getUserDateOfBirth() {
        LocalDate date = LocalDate.now();
        date = date.plusDays(3);
        date = date.minusYears(20);
        DateTimeFormatter dayOfMonth = DateTimeFormatter.ofPattern("ddMMYYYY");
        System.out.println("DateOfBirth: " + date.format(dayOfMonth));
        return String.valueOf(date.format(dayOfMonth));
    }
}


