package aventus.agis2.helpers;

import aventus.agis2.pages.Page_157_Credit7_1stCredit_IssueAndRepaymentLoan_Step1;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_157_Credit7_1stCredit_IssueAndRepaymentLoan_Step1 extends AbsractHelper {

    Page_157_Credit7_1stCredit_IssueAndRepaymentLoan_Step1 issueAndRepaymentLoan_Step1 = new Page_157_Credit7_1stCredit_IssueAndRepaymentLoan_Step1();

    public void getIssueAndRepaymentLoan_Step1() {
        issueAndRepaymentLoan_Step1.toGiveOutLoan().
                clickSaveButtonAGIS2();
        issueAndRepaymentLoan_Step1.toCompleteTheIssuance().
                clickSaveButtonAGIS2();


    }
}
