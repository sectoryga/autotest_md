package aventus.agis2.helpers;

import aventus.agis2.pages.Page_300_Credit365_1stCredit_Registration;
import aventus.webMd.helpers.AbsractHelper;

import java.awt.*;

public class Helper_300_Credit365_1stCredit_Registration extends AbsractHelper {

    Page_300_Credit365_1stCredit_Registration Page_50_Credit365_1stCredit_Registration = new Page_300_Credit365_1stCredit_Registration();

    public void getCredit365_1stCredit() throws InterruptedException, AWTException {
        Page_50_Credit365_1stCredit_Registration.openMainPageCredit365()
                .setPriceCredit365()
                .clickGetMoneyButton()
                .setUserSurname()
                .setUserName()
                .setUserPhone()
                .setUserPassword()
                .setConsentCheckBox1()
                .setConsentCheckBox2();
                Thread.sleep(1000);
        Page_50_Credit365_1stCredit_Registration.clickContinueButton()
                .setUserIDNP()
                .setUserDateOfBirth()
                .setUserGender()
                .setUserEmail()
                .setUserIBAN()
                .clickContinueButton_Step2345()
                .waitDropDownSelectorFirstElementField()
                .waitUserMaritalStatusField()
                .setUserMaritalStatus()
                .waitUserEducationStatusField()
                .setUserEducationStatus()
                .waitUserActivityType()
                .setUserActivityType()
                .waitUserOrganizationNameField()
                .setUserOrganizationName()
                .waitUserMonthlyIncome()
                .setUserMonthlyIncome()
                .clickContinueButton_Step2345()
                .setUserDocument1()
                .setUserDocument2()
                .clickContinueButton_Step2345();
        Thread.sleep(1000);
        Page_50_Credit365_1stCredit_Registration.openNewTabInChrome();
        Page_50_Credit365_1stCredit_Registration.getSMS_UserRegistrationConfirmation();
        Thread.sleep(2000);
        driver.close();
        Page_50_Credit365_1stCredit_Registration.newTabs(driver);
        Page_50_Credit365_1stCredit_Registration.pasteSMS();
        Page_50_Credit365_1stCredit_Registration.clickSendLoanButton();
        Thread.sleep(2000);


    }
}
