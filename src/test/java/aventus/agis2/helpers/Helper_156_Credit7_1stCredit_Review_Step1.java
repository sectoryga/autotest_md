package aventus.agis2.helpers;

import aventus.agis2.pages.Page_156_Credit7_1stCredit_Review_Step1;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_156_Credit7_1stCredit_Review_Step1 extends AbsractHelper {

    Page_156_Credit7_1stCredit_Review_Step1 credit7Review = new Page_156_Credit7_1stCredit_Review_Step1();

    public void getCreditReview() throws InterruptedException {
        credit7Review.openAllApplicationsAndLoans()
                .openUserApplication();
        Thread.sleep(10000);
        credit7Review.clickBecomeVerifierButton()
                .clickSaveButtonAGIS2()
                .clickDecideButton()
                .toApproveLoan();
        credit7Review.setUserAddress()
                .clickSaveButtonAGIS2();
        credit7Review.loanApplicationConfirmationByAdminFromAdmin()
                .clickSaveButtonAGIS2();
    }
}
