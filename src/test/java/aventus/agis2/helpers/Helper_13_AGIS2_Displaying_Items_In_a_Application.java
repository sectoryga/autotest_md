package aventus.agis2.helpers;

import aventus.agis2.pages.Page_13_AGIS2_Displaying_Items_In_a_Application;
import aventus.agis2.pages.Page_156_Credit7_1stCredit_Review_Step1;
import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.applicationTabPages.ApplicationsTabPage;

public class Helper_13_AGIS2_Displaying_Items_In_a_Application extends AbsractHelper {

    ApplicationsTabPage AGIS2_Applications_Tab = new ApplicationsTabPage();
    Page_156_Credit7_1stCredit_Review_Step1 Credit7_1stCredit_Review_Step1 = new Page_156_Credit7_1stCredit_Review_Step1();
    Page_13_AGIS2_Displaying_Items_In_a_Application Displaying_Items_In_a_Application = new Page_13_AGIS2_Displaying_Items_In_a_Application();

    public void check_Displaying_Items_In_a_Application() {
        AGIS2_Applications_Tab.openApplications()
                .openApplications_checkPage()
                .openAllApplications()
                .allAplications_CheckPage();
        Credit7_1stCredit_Review_Step1.openUserApplication();
        Displaying_Items_In_a_Application.openEditPage_in_Application()
                .openAutomatic_decision_in_Application()
                .openSMS_in_Application();

    }
}





