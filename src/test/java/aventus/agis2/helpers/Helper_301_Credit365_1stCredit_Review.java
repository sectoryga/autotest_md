package aventus.agis2.helpers;

import aventus.agis2.pages.Page_301_Credit365_1stCredit_Review;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_301_Credit365_1stCredit_Review extends AbsractHelper {

    Page_301_Credit365_1stCredit_Review Page_51_Credit365_1stCredit_Review = new Page_301_Credit365_1stCredit_Review();

    public void reviewCredit365_1stCredit() throws InterruptedException {
        Page_51_Credit365_1stCredit_Review.openAllApplicationsAndLoans();
        Page_51_Credit365_1stCredit_Review.openUserApplication()
                .clickBecomeVerifierButton()
                .clickSaveButtonAGIS2()
                .clickDecideButton()
                .toApproveLoan();
        Page_51_Credit365_1stCredit_Review.setUserAddress()
                .clickSaveButtonAGIS2();
        Page_51_Credit365_1stCredit_Review.loanApplicationConfirmationByAdminFromAdmin()
                .clickSaveButtonAGIS2();

    }

}
