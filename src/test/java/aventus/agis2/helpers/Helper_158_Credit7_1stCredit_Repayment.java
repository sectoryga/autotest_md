package aventus.agis2.helpers;

import aventus.agis2.pages.Page_158_Credit7_1stCredit_Repayment;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_158_Credit7_1stCredit_Repayment extends AbsractHelper {

    Page_158_Credit7_1stCredit_Repayment Page10_Credit7_1stCredit_Repayment = new Page_158_Credit7_1stCredit_Repayment();

    public void toDoRepayment() {
        Page10_Credit7_1stCredit_Repayment.clickIncomeButton()
                .clickAddNewIncome()
                .setIncomeAmount()
                .clickPreviewButton()
                .clickApproveAndProceedToLoanEditing();
    }
}
