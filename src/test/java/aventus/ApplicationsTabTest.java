package aventus;

import aventus.webMd.helpers.applicationTabHelpers.ApplicationsTabHelper;
import org.testng.annotations.Test;

public class ApplicationsTabTest {

    ApplicationsTabHelper AGIS2_Applications_Tab = new ApplicationsTabHelper();

    @Test(skipFailedInvocations = true)
    public void Test_2_AGIS2_ApplicationsTab() {
        AGIS2_Applications_Tab.checkApplicationsTab();

    }
}
