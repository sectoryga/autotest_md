package aventus;

import aventus.agis2.Generators.*;
import aventus.agis2.XML_operations.AbstractXML;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

public class API_Create_Random_User_DataTest extends AbstractXML {

    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder;

    {
        try {
            docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    Document doc = docBuilder.newDocument();
    String userPhone = Phone_number_random_generator.getPhone();
    String first_name = Name_random_generator.getName();
    String last_name = Surname_random_generator.getSurname();
    String password = Password_random_generator.getPassword();
    String personal_id = IDNP_random_generator.getUserIDNP();
    String date_of_birth = DateOfBirth_random_generator.getUserDateOfBirth();
    String email = Email_random_generator.getUserEmail();
    String userIBAN = IBAN_random_generator.getUserIBAN();

    @Test
    public void userCreate_createXML() throws TransformerException, ParserConfigurationException, IOException, SAXException {

        Element rootElement = doc.createElement("Credit7_userInfo");
        doc.appendChild(rootElement);

        Element first_name = doc.createElement("first_name");
        first_name.appendChild(doc.createTextNode(this.first_name));
        rootElement.appendChild(first_name);

        Element last_name = doc.createElement("last_name");
        last_name.appendChild(doc.createTextNode(this.last_name));
        rootElement.appendChild(last_name);

        Element mobile_phone = doc.createElement("mobile_phone");
        mobile_phone.appendChild(doc.createTextNode(this.userPhone));
        rootElement.appendChild(mobile_phone);

        Element password = doc.createElement("password");
        password.appendChild(doc.createTextNode(this.password));
        rootElement.appendChild(password);

        Element personal_id = doc.createElement("personal_id");
        personal_id.appendChild(doc.createTextNode(this.personal_id));
        rootElement.appendChild(personal_id);

        Element date_of_birth = doc.createElement("date_of_birth");
        date_of_birth.appendChild(doc.createTextNode(this.date_of_birth));
        rootElement.appendChild(date_of_birth);

        Element email = doc.createElement("email");
        email.appendChild(doc.createTextNode(this.email));
        rootElement.appendChild(email);

        Element userIBAN = doc.createElement("userIBAN");
        userIBAN.appendChild(doc.createTextNode(this.userIBAN));
        rootElement.appendChild(userIBAN);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("./src/file.xml"));
        transformer.transform(source, result);
        System.out.println("File saved!");

        try {
            String filepath = "./src/file.xml";
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(filepath);
        } finally {
        }
// ПРОВЕРКА КАК ДОСТАТЬ ИЗ XML

        Node firstNameXML = doc.getElementsByTagName("first_name").item(0);
        String first_name_xml = firstNameXML.getTextContent();
        System.out.println("first_name_xml : " + first_name_xml);


        Node lastNameXML = doc.getElementsByTagName("last_name").item(0);
        String last_name_xml = lastNameXML.getTextContent();
        System.out.println("last_name_xml : " + last_name_xml);

        Node mobilePhoneXML = doc.getElementsByTagName("mobile_phone").item(0);
        String mobile_phone_xml = mobilePhoneXML.getTextContent();
        System.out.println("mobile_phone_xml : " + mobile_phone_xml);

        Node passwordXML = doc.getElementsByTagName("password").item(0);
        String password_xml = passwordXML.getTextContent();
        System.out.println("password : " + password_xml);

        Node personalIdXML = doc.getElementsByTagName("personal_id").item(0);
        String personal_id_xml = personalIdXML.getTextContent();
        System.out.println("personal_id : " + personal_id_xml);

        Node dateOfBirthXML = doc.getElementsByTagName("date_of_birth").item(0);
        String date_of_birth_xml = dateOfBirthXML.getTextContent();
        System.out.println("date_of_birth : " + date_of_birth_xml);

        Node emailXML = doc.getElementsByTagName("email").item(0);
        String email_xml = emailXML.getTextContent();
        System.out.println("email_xml : " + email_xml);

        Node userIBAN_xml = doc.getElementsByTagName("userIBAN").item(0);
        String userIBAN_XML = userIBAN_xml.getTextContent();
        System.out.println("userIBAN_xml : " + userIBAN_XML);
        System.out.println("DONE");

    }

}