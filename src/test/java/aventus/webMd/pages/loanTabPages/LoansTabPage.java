package aventus.webMd.pages.loanTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class LoansTabPage extends AbstractPage {

    String loansButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[2]/a/span[1]";
    String loans_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[1]/ul/li[1]/a";

    String inProcessingButton_LoansTab_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[2]/ul/li[1]/a";
    String inProcessingLoans_LoansTab_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/form/div/div[1]/table/tbody/tr[1]/td[9]/span[1]";

    String processingErrorButton_LoansTab_AdminField = "/html/body/div[1]/aside/div/section/ul/li[2]/ul/li[2]/a";
    String processingLoans_LoansTab_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[8]/span[1]";

    String activeButton_LoansTab_AdminField = "/html/body/div[1]/aside/div/section/ul/li[2]/ul/li[3]/a";
    String activeLoans_LoansTab_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[8]/span[1]";

    String extendedButton_LoansTab_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[2]/ul/li[4]/a";
    String extendedLoans_LoansTab_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[8]/span[1]";

    String overdueButton_LoansTab_AdminField = "/html/body/div[1]/aside/div/section/ul/li[2]/ul/li[5]/a";
    String overdueLoans_LoansTab_checkPage__AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[8]/span[1]";

    String repaidButton_LoansTab_AdminField = "/html/body/div[1]/aside/div/section/ul/li[2]/ul/li[6]/a";
    String repaidLoans_LoansTab_checkPage__AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[8]/span[1]";

    String soldButton_LoansTab_AdminField = "/html/body/div[1]/aside/div/section/ul/li[2]/ul/li[7]/a";
    String soldLoans_LoansTab_checkPage__AdminField = "/html/body/div[1]/div";

    public LoansTabPage openLoans() {
        waitForElementVisible(getElementBy(loansButton_AdminField));
        getElement(loansButton_AdminField).click();
        return this;
    }

    public LoansTabPage loans_checkPage() {
        waitForElementClickable(getElementBy(loans_checkPage_AdminField));
        WebElement element = getElement(loans_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public LoansTabPage openInProcessingTab() {
        waitForElementVisible(getElementBy(inProcessingButton_LoansTab_AdminField));
        getElement(inProcessingButton_LoansTab_AdminField).click();
        return this;
    }

    public LoansTabPage inProcessingTab_checkPage() {
        waitForElementClickable(getElementBy(inProcessingLoans_LoansTab_checkPage_AdminField));
        WebElement element = getElement(inProcessingLoans_LoansTab_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public LoansTabPage openProcessingErrorTab() {
        waitForElementVisible(getElementBy(processingErrorButton_LoansTab_AdminField));
        getElement(processingErrorButton_LoansTab_AdminField).click();
        return this;
    }

    public LoansTabPage processingErrorTab_checkPage() {
        waitForElementClickable(getElementBy(processingLoans_LoansTab_checkPage_AdminField));
        WebElement element = getElement(processingLoans_LoansTab_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public LoansTabPage openActiveTab() {
        waitForElementVisible(getElementBy(activeButton_LoansTab_AdminField));
        getElement(activeButton_LoansTab_AdminField).click();
        return this;
    }

    public LoansTabPage activeTab_checkPage() {
        waitForElementClickable(getElementBy(activeLoans_LoansTab_checkPage_AdminField));
        WebElement element = getElement(activeLoans_LoansTab_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public LoansTabPage openExtendedTab() {
        waitForElementVisible(getElementBy(extendedButton_LoansTab_AdminField));
        getElement(extendedButton_LoansTab_AdminField).click();
        return this;
    }

    public LoansTabPage extendedTab_checkPage() {
        waitForElementClickable(getElementBy(extendedLoans_LoansTab_checkPage_AdminField));
        WebElement element = getElement(extendedLoans_LoansTab_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public LoansTabPage openOverdueTab() {
        waitForElementVisible(getElementBy(overdueButton_LoansTab_AdminField));
        getElement(overdueButton_LoansTab_AdminField).click();
        return this;
    }

    public LoansTabPage overdueTab_checkPage() {
        waitForElementClickable(getElementBy(overdueLoans_LoansTab_checkPage__AdminField));
        WebElement element = getElement(overdueLoans_LoansTab_checkPage__AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public LoansTabPage openRepaidTab() {
        waitForElementVisible(getElementBy(repaidButton_LoansTab_AdminField));
        getElement(repaidButton_LoansTab_AdminField).click();
        return this;
    }

    public LoansTabPage repaidTab_checkPage() {
        waitForElementClickable(getElementBy(repaidLoans_LoansTab_checkPage__AdminField));
        WebElement element = getElement(repaidLoans_LoansTab_checkPage__AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public LoansTabPage openSoldRepaidTab() {
        waitForElementVisible(getElementBy(soldButton_LoansTab_AdminField));
        getElement(soldButton_LoansTab_AdminField).click();
        return this;
    }

    public LoansTabPage soldTab_checkPage() {
        waitForElementClickable(getElementBy(soldLoans_LoansTab_checkPage__AdminField));
        WebElement element = getElement(soldLoans_LoansTab_checkPage__AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
