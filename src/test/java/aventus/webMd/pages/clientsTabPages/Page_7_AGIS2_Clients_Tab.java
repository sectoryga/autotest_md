package aventus.webMd.pages.clientsTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_7_AGIS2_Clients_Tab extends AbstractPage {

    String clientsButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[6]/a/span[1]";
    String clients_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[6]/ul/li[1]/a";

    String allClients_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[6]/ul/li[1]/a";
    String allClients_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String repeatedRegistrations_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[6]/ul/li[2]/a";
    String repeatedRegistrations_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String noApplications_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[6]/ul/li[3]/a";
    String noApplications_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[6]/ul/li[3]/a";

    String blackList_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[6]/ul/li[4]/a";
    String blackList_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    public Page_7_AGIS2_Clients_Tab openClients() {
        waitForElementVisible(getElementBy(clientsButton_AdminField));
        getElement(clientsButton_AdminField).click();
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openClients_checkPage() {
        waitForElementClickable(getElementBy(clients_checkPage_AdminField));
        WebElement element = getElement(clients_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openAllClients() {
        waitForElementVisible(getElementBy(allClients_AdminField));
        getElement(allClients_AdminField).click();
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openAllClients_checkPage() {
        waitForElementClickable(getElementBy(allClients_checkPage_AdminField));
        WebElement element = getElement(allClients_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openRepeatedRegistrations() {
        waitForElementVisible(getElementBy(repeatedRegistrations_AdminField));
        getElement(repeatedRegistrations_AdminField).click();
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openRepeatedRegistrations_checkPage() {
        waitForElementClickable(getElementBy(repeatedRegistrations_checkPage_AdminField));
        WebElement element = getElement(repeatedRegistrations_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openNoApplications() {
        waitForElementVisible(getElementBy(noApplications_AdminField));
        getElement(noApplications_AdminField).click();
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openNoApplications_checkPage() {
        waitForElementClickable(getElementBy(noApplications_checkPage_AdminField));
        WebElement element = getElement(noApplications_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openBlackList() {
        waitForElementVisible(getElementBy(blackList_AdminField));
        getElement(blackList_AdminField).click();
        return this;
    }

    public Page_7_AGIS2_Clients_Tab openBlackList_checkPage() {
        waitForElementClickable(getElementBy(blackList_checkPage_AdminField));
        WebElement element = getElement(blackList_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }


}
