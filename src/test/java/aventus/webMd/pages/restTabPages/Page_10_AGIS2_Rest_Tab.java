package aventus.webMd.pages.restTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_10_AGIS2_Rest_Tab extends AbstractPage {

    String restButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/a/span[1]";
    String restButton_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[1]/a";

    String discounts_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[1]/a";
    String discounts_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String priceLists_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[2]/a";
    String priceLists_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String commentStyle_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[3]/a";
    String commentStyle_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String smsSenders_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[4]/a";
    String smsSenders_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/form/div/div[1]/table/tbody/tr[2]/td[4]";

    String statements_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[5]/a";
    String statements_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/thead/tr/th[4]/a";

    String documentTemplates_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[6]/a";
    String documentTemplates_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div/div/div/div[1]/table/thead/tr/th[6]/a";

    String autoAssignmentOfVerifiers_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[9]/ul/li[7]/a";
    String autoAssignmentOfVerifiers_checkPage_AdminField = "/html/body/div[1]/div/section[1]/div/nav/div/div/div/ul/li[1]/a";

    public Page_10_AGIS2_Rest_Tab openRestTab() {
        waitForElementVisible(getElementBy(restButton_AdminField));
        getElement(restButton_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab restTab_checkPage() {
        waitForElementClickable(getElementBy(restButton_checkPage_AdminField));
        WebElement element = getElement(restButton_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_10_AGIS2_Rest_Tab openDiscounts() {
        waitForElementVisible(getElementBy(discounts_AdminField));
        getElement(discounts_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab discounts_checkPage() {
        waitForElementClickable(getElementBy(discounts_checkPage_AdminField));
        WebElement element = getElement(discounts_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_10_AGIS2_Rest_Tab openPriceLists() {
        waitForElementVisible(getElementBy(priceLists_AdminField));
        getElement(priceLists_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab priceLists_checkPage() {
        waitForElementClickable(getElementBy(priceLists_checkPage_AdminField));
        WebElement element = getElement(priceLists_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_10_AGIS2_Rest_Tab openCommentStyle() {
        waitForElementVisible(getElementBy(commentStyle_AdminField));
        getElement(commentStyle_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab commentStyle_checkPage() {
        waitForElementClickable(getElementBy(commentStyle_checkPage_AdminField));
        WebElement element = getElement(commentStyle_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_10_AGIS2_Rest_Tab openSmsSenders() {
        waitForElementVisible(getElementBy(smsSenders_AdminField));
        getElement(smsSenders_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab smsSenders_checkPage() {
        waitForElementClickable(getElementBy(smsSenders_checkPage_AdminField));
        WebElement element = getElement(smsSenders_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_10_AGIS2_Rest_Tab openStatements() {
        waitForElementVisible(getElementBy(statements_AdminField));
        getElement(statements_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab statements_checkPage() {
        waitForElementClickable(getElementBy(statements_checkPage_AdminField));
        WebElement element = getElement(statements_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }


    public Page_10_AGIS2_Rest_Tab openDocumentTemplates() {
        waitForElementVisible(getElementBy(documentTemplates_AdminField));
        getElement(documentTemplates_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab documentTemplates_checkPage() {
        waitForElementClickable(getElementBy(documentTemplates_checkPage_AdminField));
        WebElement element = getElement(documentTemplates_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_10_AGIS2_Rest_Tab openAutoAssignmentOfVerifiers() {
        waitForElementVisible(getElementBy(autoAssignmentOfVerifiers_AdminField));
        getElement(autoAssignmentOfVerifiers_AdminField).click();
        return this;
    }

    public Page_10_AGIS2_Rest_Tab autoAssignmentOfVerifiers_checkPage() {
        waitForElementClickable(getElementBy(autoAssignmentOfVerifiers_checkPage_AdminField));
        WebElement element = getElement(autoAssignmentOfVerifiers_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
