package aventus.webMd.pages.contactCenterTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_8_AGIS2_ContactCenter_Tab extends AbstractPage {

    String contactCenterButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[7]/a/span[1]";
    String contactCenterButton_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[7]/ul/li[1]/a";

    String contactCenter_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[7]/ul/li[1]/a";
    String contactCenter_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String history_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[7]/ul/li[2]/a";
    String history_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String settings_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[7]/ul/li[3]/a";
    String settings_checkPage_AdminField = "/html/body/div[1]/div/section[1]/div/nav/div/div/div/ul/li[2]/a";

    String onlineReport_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[7]/ul/li[4]/a";
    String onlineReport_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String contactsReport_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[7]/ul/li[5]/a";
    String contactsReport_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";


    public Page_8_AGIS2_ContactCenter_Tab openContactCenterTab() {
        waitForElementVisible(getElementBy(contactCenterButton_AdminField));
        getElement(contactCenterButton_AdminField).click();
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openContactCenterTab_checkPage() {
        waitForElementClickable(getElementBy(contactCenterButton_checkPage_AdminField));
        WebElement element = getElement(contactCenterButton_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openContactCenter() {
        waitForElementVisible(getElementBy(contactCenter_AdminField));
        getElement(contactCenter_AdminField).click();
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openContactCenter_checkPage() {
        waitForElementClickable(getElementBy(contactCenter_checkPage_AdminField));
        WebElement element = getElement(contactCenter_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openHistory() {
        waitForElementVisible(getElementBy(history_AdminField));
        getElement(history_AdminField).click();
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openHistory_checkPage() {
        waitForElementClickable(getElementBy(history_checkPage_AdminField));
        WebElement element = getElement(history_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openSettings() {
        waitForElementVisible(getElementBy(settings_AdminField));
        getElement(settings_AdminField).click();
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openSettings_checkPage() {
        waitForElementClickable(getElementBy(settings_checkPage_AdminField));
        WebElement element = getElement(settings_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openOnlineReport() {
        waitForElementVisible(getElementBy(onlineReport_AdminField));
        getElement(onlineReport_AdminField).click();
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openOnlineReport_checkPage() {
        waitForElementClickable(getElementBy(onlineReport_checkPage_AdminField));
        WebElement element = getElement(onlineReport_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openContactsReport() {
        waitForElementVisible(getElementBy(contactsReport_AdminField));
        getElement(contactsReport_AdminField).click();
        return this;
    }

    public Page_8_AGIS2_ContactCenter_Tab openContactsReport_checkPage() {
        waitForElementClickable(getElementBy(contactsReport_checkPage_AdminField));
        WebElement element = getElement(contactsReport_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
