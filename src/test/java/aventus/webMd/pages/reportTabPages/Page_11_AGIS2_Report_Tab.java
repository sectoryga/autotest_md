package aventus.webMd.pages.reportTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_11_AGIS2_Report_Tab extends AbstractPage {

    String reportButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/a/span[1]";
    String reportButton_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[1]/a";

    String affiliateUsers_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[1]/a";
    String affiliateUsers_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String affiliateCredits_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[2]/a";
    String affiliateCredits_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String affiliateRequests_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[2]/a";
    String affiliateRequests_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String summaryReport_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[4]/a";
    String summaryReport_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String cashflowReport_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[5]/a";
    String cashflowReport_checkPage_AdminField = "//*[@id=\"cashflow_report\"]/div[1]/form/div/div/div/div/div[3]/button";

    String incomeReport_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[6]/a";
    String incomeReport_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/thead/tr/th[7]";

    String reportProcessor_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[7]/a";
    String reportProcessor_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String universalReport_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[8]/a";
    String universalReport_checkPage_AdminField = "//*[@id=\"form__submit\"]";

    String smsReport_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[10]/ul/li[9]/a";
    String smsReport_checkPage_AdminField = "/html/body/div[1]/div/section[2]/table/thead/tr/td[2]/b";


    public Page_11_AGIS2_Report_Tab openReportTab() {
        waitForElementVisible(getElementBy(reportButton_AdminField));
        getElement(reportButton_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab reportTab_checkPage() {
        waitForElementClickable(getElementBy(reportButton_checkPage_AdminField));
        WebElement element = getElement(reportButton_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openAffiliateUsers() {
        waitForElementVisible(getElementBy(affiliateUsers_AdminField));
        getElement(affiliateUsers_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab affiliateUsers_checkPage() {
        waitForElementClickable(getElementBy(affiliateUsers_checkPage_AdminField));
        WebElement element = getElement(affiliateUsers_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openAffiliateСredits() {
        waitForElementVisible(getElementBy(affiliateCredits_AdminField));
        getElement(affiliateCredits_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab affiliateСredits_checkPage() {
        waitForElementClickable(getElementBy(affiliateCredits_checkPage_AdminField));
        WebElement element = getElement(affiliateCredits_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openAffiliateRequests() {
        waitForElementVisible(getElementBy(affiliateRequests_AdminField));
        getElement(affiliateRequests_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab affiliateRequests_checkPage() {
        waitForElementClickable(getElementBy(affiliateRequests_checkPage_AdminField));
        WebElement element = getElement(affiliateRequests_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openSummaryReport() {
        waitForElementVisible(getElementBy(summaryReport_AdminField));
        getElement(summaryReport_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab summaryReport_checkPage() {
        waitForElementClickable(getElementBy(summaryReport_checkPage_AdminField));
        WebElement element = getElement(summaryReport_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openCashflowReport() {
        waitForElementVisible(getElementBy(cashflowReport_AdminField));
        getElement(cashflowReport_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab cashflowReport_checkPage() {
        waitForElementClickable(getElementBy(cashflowReport_checkPage_AdminField));
        WebElement element = getElement(cashflowReport_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openIncomeReport() {
        waitForElementVisible(getElementBy(incomeReport_AdminField));
        getElement(incomeReport_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab incomeReport_checkPage() {
        waitForElementClickable(getElementBy(incomeReport_checkPage_AdminField));
        WebElement element = getElement(incomeReport_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openReportProcessor() {
        waitForElementVisible(getElementBy(reportProcessor_AdminField));
        getElement(reportProcessor_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab reportProcessor_checkPage() {
        waitForElementClickable(getElementBy(reportProcessor_checkPage_AdminField));
        WebElement element = getElement(reportProcessor_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openUniversalReport() {
        waitForElementVisible(getElementBy(universalReport_AdminField));
        getElement(universalReport_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab universalReport_checkPage() {
        waitForElementClickable(getElementBy(universalReport_checkPage_AdminField));
        WebElement element = getElement(universalReport_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_11_AGIS2_Report_Tab openSmsReport() {
        waitForElementVisible(getElementBy(smsReport_AdminField));
        getElement(smsReport_AdminField).click();
        return this;
    }

    public Page_11_AGIS2_Report_Tab smsReport_checkPage() {
        waitForElementClickable(getElementBy(smsReport_checkPage_AdminField));
        WebElement element = getElement(smsReport_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
