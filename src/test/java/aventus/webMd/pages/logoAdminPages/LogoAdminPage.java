package aventus.webMd.pages.logoAdminPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class LogoAdminPage extends AbstractPage {

    String AdminUserNameField = "username";
    String AdminUserPasswordField = "password";
    String submitAdminButton = "submit";
    private String logo = "/html/body/div[1]/header/a/img";

    public LogoAdminPage openMainPageAGIS2() {
        openUrl(AGIS2_URL);
        return this;
    }

    public LogoAdminPage waitAdminUserNameField() {
        waitForElementClickable(getElementById(AdminUserNameField));
        return this;
    }

    public LogoAdminPage setAdminUserName() {
        waitAdminUserNameField();
        getElementId(AdminUserNameField).sendKeys(adminUserName);
        return this;
    }

    public LogoAdminPage waitAdminUserPassword() {
        waitForElementClickable(getElementById(AdminUserPasswordField));
        return this;
    }

    public LogoAdminPage setAdminUserPassword() {
        waitAdminUserPassword();
        getElementId(AdminUserPasswordField).sendKeys(adminUserPassword);
        return this;
    }

    public LogoAdminPage waitSubmitAdminButton() {
        waitForElementClickable(getElementById(submitAdminButton));
        return this;
    }

    public LogoAdminPage clickLoginButtonAGIS2() {
        waitSubmitAdminButton();
        getElementId(submitAdminButton).click();
        return this;
    }

    public LogoAdminPage checkLogo() {
        WebElement element = getElement(logo);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
