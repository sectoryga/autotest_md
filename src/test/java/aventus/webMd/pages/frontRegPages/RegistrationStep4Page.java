package aventus.webMd.pages.frontRegPages;


import aventus.agis2.pages.AbstractPage;

public class RegistrationStep4Page extends AbstractPage {

    String dropDownMaritalStatusField = "//*[@data-id='marital_status']";
    String dropDownEducationField = "//*[@data-id='education']";
    String dropDownActivityField = "//*[@data-id='activity_type']";
    String organizationNameField = "//*[@data-id='organization_name']";
    String dropDownMonthlyIncomeField = "//*[@data-id='monthly_income']";
    String dropDownSelectorFirstElement = ".ng-option.ng-star-inserted.ng-option-marked";
    String ContinueButtonField = "button";

    public RegistrationStep4Page waitDropDownSelectorFirstElementField() {
        waitForElementClickable(getElementByCss(dropDownSelectorFirstElement));
        return this;
    }

    public RegistrationStep4Page waitUserMaritalStatusField() {
        waitForElementClickable(getElementBy(dropDownMaritalStatusField));
        return this;
    }

    public RegistrationStep4Page setUserMaritalStatus() {
        waitUserMaritalStatusField().getElement(dropDownMaritalStatusField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public RegistrationStep4Page waitUserEducationStatusField() {
        waitForElementClickable(getElementBy(dropDownEducationField));
        return this;
    }

    public RegistrationStep4Page setUserEducationStatus() {
        waitUserEducationStatusField().getElement(dropDownEducationField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public RegistrationStep4Page waitUserActivityType() {
        waitForElementClickable(getElementBy(dropDownActivityField));
        return this;
    }

    public RegistrationStep4Page setUserActivityType() {
        waitUserActivityType();
        getElement(dropDownActivityField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public RegistrationStep4Page waitUserOrganizationNameField() {
        waitForElementClickable(getElementBy(organizationNameField));
        return this;
    }

    public RegistrationStep4Page setUserOrganizationName() {
        waitUserOrganizationNameField();
        getElement(organizationNameField).sendKeys("aventus");
        return this;
    }

    public RegistrationStep4Page waitUserMonthlyIncome() {
        waitForElementClickable(getElementBy(dropDownMonthlyIncomeField));
        return this;
    }

    public RegistrationStep4Page setUserMonthlyIncome() {
        waitUserMonthlyIncome();
        getElement(dropDownMonthlyIncomeField).click();
        waitDropDownSelectorFirstElementField().getElementCss(dropDownSelectorFirstElement).click();
        return this;
    }

    public RegistrationStep4Page clickContinueButton(){
        waitForElementVisible(getElementByTag(ContinueButtonField));
        getElementTag(ContinueButtonField).click();
        return this;
    }
}
