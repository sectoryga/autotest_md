package aventus.webMd.pages.frontRegPages;


import aventus.agis2.Generators.Name_random_generator;
import aventus.agis2.Generators.Password_random_generator;
import aventus.agis2.Generators.Phone_number_random_generator;
import aventus.agis2.Generators.Surname_random_generator;
import aventus.agis2.pages.AbstractPage;


public class RegistrationStep2Page extends AbstractPage {

    String userSurnameField = "//*[@data-id='last_name']";
    String userNameField = "//*[@data-id='first_name']";
    String userPhoneField = "//*[@data-id='mobile_phone']";
    String userPasswordField = "//*[@data-id='password']";
    String checkBox1 = "//form-control[5]//div[1]//form-control-checkbox[1]//div[1]//label[1]//span[1]";
    String checkBox2 = "//form-control[6]//div[1]//form-control-checkbox[1]//div[1]//label[1]//span[1]";
    String ContinueButtonField = "/html/body/app-root/app-global-wrapper/app-application-wrapper/main/section/div/div/app-registration-page/div[2]/app-registration-form/form/div/form-control/div/form-button/button";


    public RegistrationStep2Page waitUserSurnameField() throws InterruptedException {
        Thread.sleep(2000);
        waitForElementClickable(getElementBy(userSurnameField));
        return this;
    }

    public RegistrationStep2Page setUserSurname() throws InterruptedException {
        waitUserSurnameField();
        getElement(userSurnameField).sendKeys(Surname_random_generator.getSurname());
        return this;
    }

    public RegistrationStep2Page waitUserNameField() {
        waitForElementClickable(getElementBy(userNameField));
        return this;
    }

    public RegistrationStep2Page setUserName() {
        waitUserNameField();
        getElement(userNameField).sendKeys(Name_random_generator.getName());
        return this;
    }

    public RegistrationStep2Page waitUserPhoneField() {
        waitForElementClickable(getElementBy(userPhoneField));
        return this;
    }

    public RegistrationStep2Page setUserPhone() {
        waitUserPhoneField();
        String userPhone = Phone_number_random_generator.getPhone();
        getElement(userPhoneField).sendKeys(userPhone);
        return this;
    }

    public RegistrationStep2Page waitUserPasswordField() {
        waitForElementClickable(getElementBy(userPasswordField));
        return this;
    }

    public RegistrationStep2Page setUserPassword() {
        waitUserPasswordField();
        getElement(userPasswordField).sendKeys(Password_random_generator.getPassword());
        return this;
    }

    public RegistrationStep2Page waitCheckBox1Field() {
        waitForElementClickable(getElementBy(checkBox1));
        return this;
    }

    public RegistrationStep2Page setConsentCheckBox1() {
        waitCheckBox1Field();
        getElement(checkBox1).click();
        return this;
    }

    public RegistrationStep2Page waitCheckBox2Field() {
        waitForElementClickable(getElementBy(checkBox2));
        return this;
    }

    public RegistrationStep2Page setConsentCheckBox2() {
        waitCheckBox2Field();
        getElement(checkBox2).click();
        return this;
    }

    public RegistrationStep2Page clickContinueButton(){
        waitForElementVisible(getElementBy(ContinueButtonField));
        getElement(ContinueButtonField).click();
        return this;
    }
}
