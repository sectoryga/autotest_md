package aventus.webMd.pages.frontRegPages;

import aventus.agis2.Generators.DateOfBirth_random_generator;
import aventus.agis2.Generators.Email_random_generator;
import aventus.agis2.Generators.IBAN_random_generator;
import aventus.agis2.Generators.IDNP_random_generator;
import aventus.agis2.pages.AbstractPage;

public class RegistrationStep3Page extends AbstractPage {


    String userIDNPField = "//*[@data-id='personal_id']";
    String userDateOfBirthfield = "//*[@data-id='date_of_birth']";
    String userGenderField = "app-checkbox-label";
    String userEmailField = "//*[@data-id='email']";
    String userIBAN = "//*[@data-id='btn-bank_account_number']";
    String ContinueButtonField = "button";
    public String idnp;

    public RegistrationStep3Page waitUserIDNPField() {
        waitForElementClickable(getElementBy(userIDNPField));
        return this;
    }

    public RegistrationStep3Page setUserIDNP() {
        waitUserIDNPField();
        String idnp = IDNP_random_generator.getUserIDNP();
        getElement(userIDNPField).sendKeys(idnp);
        this.idnp = idnp;
        return this;
    }

    public RegistrationStep3Page waitUserDateOfBirthField() {
        waitForElementClickable(getElementBy(userDateOfBirthfield));
        return this;
    }

    public RegistrationStep3Page setUserDateOfBirth() {
        waitUserDateOfBirthField();
        getElement(userDateOfBirthfield).sendKeys(DateOfBirth_random_generator.getUserDateOfBirth());
        return this;
    }

    public RegistrationStep3Page waitUserGenderField() {
        waitForElementClickable(getElementByClass(userGenderField));
        return this;
    }

    public RegistrationStep3Page setUserGender() {
        waitUserGenderField();
        getElementC(userGenderField).click();
        return this;
    }

    public RegistrationStep3Page waitUserEmailField() {
        waitForElementClickable(getElementBy(userEmailField));
        return this;
    }

    public RegistrationStep3Page setUserEmail() {
        waitUserEmailField();
        getElement(userEmailField).sendKeys(Email_random_generator.getUserEmail());
        return this;
    }

    public RegistrationStep3Page waitUserIBANField() {
        waitForElementClickable(getElementBy(userIBAN));
        return this;
    }

    public RegistrationStep3Page setUserIBAN() {
        waitUserIBANField();
        getElement(userIBAN).sendKeys(IBAN_random_generator.getUserIBAN());
        return this;
    }

    public RegistrationStep3Page clickContinueButton(){
        getElementTag(ContinueButtonField).click();
        return this;
    }
}