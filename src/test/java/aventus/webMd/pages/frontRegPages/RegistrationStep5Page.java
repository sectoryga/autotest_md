package aventus.webMd.pages.frontRegPages;

import aventus.agis2.pages.AbstractPage;

import java.io.File;

public class RegistrationStep5Page extends AbstractPage {

    public String uploadDocument1Field = "//*[@data-id='upload-front']";
    public String uploadDocument2Field = "//*[@data-id='upload-back']";
    String ContinueButtonField = "button";

    public File userDocument_2 = new File("./src/photoTwo.jpg");
    public File userDocument_1 = new File("./src/photoOne.jpg");


    public RegistrationStep5Page waitPhoto1() {
        waitForElementVisible(getElementBy(uploadDocument1Field));
        return this;
    }

    public RegistrationStep5Page setUserDocument1() {
        waitPhoto1()
                .getElement(uploadDocument1Field)
                .sendKeys(userDocument_1.getAbsolutePath());
        return this;
    }

    public RegistrationStep5Page waitPhoto2() {
        waitForElementVisible(getElementBy(uploadDocument1Field));
        waitForElementVisible(getElementBy(uploadDocument2Field));
        return this;
    }

    public RegistrationStep5Page setUserDocument2() throws InterruptedException {
        Thread.sleep(1000);
        waitPhoto2().
                getElement(uploadDocument2Field)
                .sendKeys(userDocument_2.getAbsolutePath());
        return this;
    }

    public RegistrationStep5Page clickContinueButton() {
        waitForElementVisible(getElementByTag(ContinueButtonField));
        waitForElementClickable(getElementByTag(ContinueButtonField));
        getElementTag(ContinueButtonField).click();
        return this;
    }
}
