package aventus.webMd.pages.frontRegPages;

import aventus.agis2.pages.AbstractPage;

public class RegistrationStep1Page extends AbstractPage {

    String setPriceField = "//input[@class='calculate__price-input']";
    String getMoneyButton = "//*[@id=\"calculator\"]/div[3]/a";

    public RegistrationStep1Page openMainPageCredit7() {
        openUrl(Credit7Url);
        return this;
    }

    public RegistrationStep1Page setPriceCredit7(){
        getElement(setPriceField).sendKeys("\b\b\b\b\b\b");
        getElement(setPriceField).sendKeys("7000");
        return this;
    }

    public RegistrationStep1Page clickGetMoneyButton(){
        waitForElementClickable(getElementBy(getMoneyButton));
        getElement(getMoneyButton).click();
        return this;
    }
}

//*[@id="calculator"]/div[3]/a