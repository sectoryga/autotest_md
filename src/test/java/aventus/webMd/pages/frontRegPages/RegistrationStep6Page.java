package aventus.webMd.pages.frontRegPages;

import aventus.agis2.pages.AbstractPage;

public class RegistrationStep6Page extends AbstractPage {

    String smsClientConfirmationAgis2Field = "mark";
    String smsConfirmationField = "//*[@data-id='code']";
    String getSendLoanButtonField = "//*[@data-id='btn-continue']";
    String messageField = "messageBlock";
    String checkLoanButtonField = ".pa-nav__link";
    private String sms;

    public String getSMS_UserRegistrationConfirmation() {
        openUrl(AGIS2_allUsersURL);
        String sms = getElementTag(smsClientConfirmationAgis2Field).getText();
        System.out.println(sms);
        driver.close();

        this.sms = sms;
        return sms;
    }

    public RegistrationStep6Page pasteSMS() {
        getElement(smsConfirmationField).click();
        getElement(smsConfirmationField).sendKeys(sms);
        return this;
    }

    public RegistrationStep6Page clickSendLoanButton() {
        waitForElementClickable(getElementBy(getSendLoanButtonField));
        getElement(getSendLoanButtonField).click();
        waitForElementVisible(getElementById(messageField));
        waitForElementClickable(getElementByCss(checkLoanButtonField));
        return this;
    }

}
