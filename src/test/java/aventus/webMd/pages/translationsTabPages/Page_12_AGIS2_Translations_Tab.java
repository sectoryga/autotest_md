package aventus.webMd.pages.translationsTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_12_AGIS2_Translations_Tab extends AbstractPage {

    String translationsButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[11]/a/span[1]";

    String smsTemplates_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[11]/ul/li[1]/a";
    String smsTemplates_checkPage_AdminField = "/html/body/div[1]/div/section[1]/div/nav/div/div";

    String emailTemplates_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[11]/ul/li[2]/a";
    String emailTemplates_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String sitePages_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[11]/ul/li[3]/a";
    String sitePages_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String faq_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[11]/ul/li[4]/a";
    String faq_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    public Page_12_AGIS2_Translations_Tab openTranslationsButton() {
        waitForElementVisible(getElementBy(translationsButton_AdminField));
        getElement(translationsButton_AdminField).click();
        return this;
    }

    public Page_12_AGIS2_Translations_Tab openSmsTemplates() {
        waitForElementVisible(getElementBy(smsTemplates_AdminField));
        getElement(smsTemplates_AdminField).click();
        return this;
    }

    public Page_12_AGIS2_Translations_Tab smsTemplates_checkPage() {
        waitForElementClickable(getElementBy(smsTemplates_checkPage_AdminField));
        WebElement element = getElement(smsTemplates_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_12_AGIS2_Translations_Tab openEmailTemplates() {
        waitForElementVisible(getElementBy(emailTemplates_AdminField));
        getElement(emailTemplates_AdminField).click();
        return this;
    }

    public Page_12_AGIS2_Translations_Tab emailTemplates_checkPage() {
        waitForElementClickable(getElementBy(emailTemplates_checkPage_AdminField));
        WebElement element = getElement(emailTemplates_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_12_AGIS2_Translations_Tab openSitePages() {
        waitForElementVisible(getElementBy(sitePages_AdminField));
        getElement(sitePages_AdminField).click();
        return this;
    }

    public Page_12_AGIS2_Translations_Tab sitePages_checkPage() {
        waitForElementClickable(getElementBy(sitePages_checkPage_AdminField));
        WebElement element = getElement(sitePages_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_12_AGIS2_Translations_Tab openFAQ() {
        waitForElementVisible(getElementBy(faq_AdminField));
        getElement(faq_AdminField).click();
        return this;
    }

    public Page_12_AGIS2_Translations_Tab faq_checkPage() {
        waitForElementClickable(getElementBy(faq_checkPage_AdminField));
        WebElement element = getElement(faq_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
