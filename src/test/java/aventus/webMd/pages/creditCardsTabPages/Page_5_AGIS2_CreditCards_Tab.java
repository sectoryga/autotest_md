package aventus.webMd.pages.creditCardsTabPages;

import aventus.agis2.pages.AbstractPage;

public class Page_5_AGIS2_CreditCards_Tab extends AbstractPage {

    String creditCardsTab_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[4]/a/span[1]";
    String creditCardsTab_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[4]/ul/li[1]/a";

    String cardTransactions1_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[4]/ul/li[1]/a";
    String cardTransactions1_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String cardVerification_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[4]/ul/li[2]/a";
    String cardVerification_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String cardTransactions2_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[4]/ul/li[3]/a";
    String cardTransactions2_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    public Page_5_AGIS2_CreditCards_Tab openCreditCardsTab() {
        waitForElementVisible(getElementBy(creditCardsTab_AdminField));
        getElement(creditCardsTab_AdminField).click();
        return this;
    }

    public Page_5_AGIS2_CreditCards_Tab creditCardsTab_checkPage() {
        waitForElementVisible(getElementBy(creditCardsTab_checkPage_AdminField));
        getElement(creditCardsTab_checkPage_AdminField).click();
        return this;
    }

    public Page_5_AGIS2_CreditCards_Tab openCardTransactions1() {
        waitForElementVisible(getElementBy(cardTransactions1_AdminField));
        getElement(cardTransactions1_AdminField).click();
        return this;
    }

    public Page_5_AGIS2_CreditCards_Tab cardTransactions1_checkPage() {
        waitForElementVisible(getElementBy(cardTransactions1_checkPage_AdminField));
        getElement(cardTransactions1_checkPage_AdminField).click();
        return this;
    }

    public Page_5_AGIS2_CreditCards_Tab openCardVerification() {
        waitForElementVisible(getElementBy(cardVerification_AdminField));
        getElement(cardVerification_AdminField).click();
        return this;
    }

    public Page_5_AGIS2_CreditCards_Tab cardVerification_checkPage() {
        waitForElementVisible(getElementBy(cardVerification_checkPage_AdminField));
        getElement(cardVerification_checkPage_AdminField).click();
        return this;
    }

    public Page_5_AGIS2_CreditCards_Tab openCardTransactions2() {
        waitForElementVisible(getElementBy(cardTransactions2_AdminField));
        getElement(cardTransactions2_AdminField).click();
        return this;
    }

    public Page_5_AGIS2_CreditCards_Tab cardTransactions2_checkPage() {
        waitForElementVisible(getElementBy(cardTransactions2_checkPage_AdminField));
        getElement(cardTransactions2_checkPage_AdminField).click();
        return this;
    }

}
