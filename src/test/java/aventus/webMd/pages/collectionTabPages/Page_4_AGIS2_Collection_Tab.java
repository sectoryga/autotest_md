package aventus.webMd.pages.collectionTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_4_AGIS2_Collection_Tab extends AbstractPage {

    String collectionTabButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[3]/a/span[1]";

    String collection_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[3]/ul/li[1]/a";
    String collection_checkPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/form/div/div[1]/table/thead/tr/th[10]";

    String collectionInhouse_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[3]/ul/li[2]/a";
    String collectionInhouse_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String collectionOutsource_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[3]/ul/li[3]/a";
    String collectionOutsource_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String collectionLegal_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[3]/ul/li[4]/a";
    String collectionLegal_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String collectionIncome_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[3]/ul/li[5]/a";
    String collectionIncome_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String collectionSetCollectorsList_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[3]/ul/li[6]/a";
    String collectionSetCollectorsList_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    public Page_4_AGIS2_Collection_Tab openCollectionTab() {
        waitForElementVisible(getElementBy(collectionTabButton_AdminField));
        getElement(collectionTabButton_AdminField).click();
        return this;
    }

    public Page_4_AGIS2_Collection_Tab openCollection() {
        waitForElementVisible(getElementBy(collection_AdminField));
        getElement(collection_AdminField).click();
        return this;
    }

    public Page_4_AGIS2_Collection_Tab collection_checkPage() {
        waitForElementClickable(getElementBy(collection_checkPage_AdminField));
        WebElement element = getElement(collection_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_4_AGIS2_Collection_Tab openCollectionInhouse() {
        waitForElementVisible(getElementBy(collectionInhouse_AdminField));
        getElement(collectionInhouse_AdminField).click();
        return this;
    }

    public Page_4_AGIS2_Collection_Tab collectionInhouse_checkPage() {
        waitForElementClickable(getElementBy(collectionInhouse_checkPage_AdminField));
        WebElement element = getElement(collectionInhouse_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_4_AGIS2_Collection_Tab openCollectionOutsource() {
        waitForElementVisible(getElementBy(collectionOutsource_AdminField));
        getElement(collectionOutsource_AdminField).click();
        return this;
    }

    public Page_4_AGIS2_Collection_Tab collectionOutsource_checkPage() {
        waitForElementClickable(getElementBy(collectionOutsource_checkPage_AdminField));
        WebElement element = getElement(collectionOutsource_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_4_AGIS2_Collection_Tab openCollectionLegal() {
        waitForElementVisible(getElementBy(collectionLegal_AdminField));
        getElement(collectionLegal_AdminField).click();
        return this;
    }

    public Page_4_AGIS2_Collection_Tab collectionLegal_checkPage() {
        waitForElementClickable(getElementBy(collectionLegal_checkPage_AdminField));
        WebElement element = getElement(collectionLegal_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_4_AGIS2_Collection_Tab openCollectionIncome() {
        waitForElementVisible(getElementBy(collectionIncome_AdminField));
        getElement(collectionIncome_AdminField).click();
        return this;
    }

    public Page_4_AGIS2_Collection_Tab collectionIncome_checkPage() {
        waitForElementClickable(getElementBy(collectionIncome_checkPage_AdminField));
        WebElement element = getElement(collectionIncome_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_4_AGIS2_Collection_Tab openCollectionSetCollectorsList() {
        waitForElementVisible(getElementBy(collectionSetCollectorsList_AdminField));
        getElement(collectionSetCollectorsList_AdminField).click();
        return this;
    }

    public Page_4_AGIS2_Collection_Tab collectionSetCollectorsList_checkPage() {
        waitForElementClickable(getElementBy(collectionSetCollectorsList_checkPage_AdminField));
        WebElement element = getElement(collectionSetCollectorsList_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
