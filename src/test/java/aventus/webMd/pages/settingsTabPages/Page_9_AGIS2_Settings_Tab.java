package aventus.webMd.pages.settingsTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class Page_9_AGIS2_Settings_Tab extends AbstractPage {

    String settingsButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[8]/a/span[1]";
    String settingsButton_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[8]/ul/li[1]/a";

    String administrators_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[8]/ul/li[1]/a";
    String administrators_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String globalSettings_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[8]/ul/li[2]/a";
    String globalSettings_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String affiliates_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[8]/ul/li[3]/a";
    String affiliates_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String settingReminders_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[8]/ul/li[4]/a";
    String settingReminders_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String areas_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[8]/ul/li[5]/a";
    String areas_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    public Page_9_AGIS2_Settings_Tab openSettingsTab() {
        waitForElementVisible(getElementBy(settingsButton_AdminField));
        getElement(settingsButton_AdminField).click();
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openSettingsTab_checkPage() {
        waitForElementClickable(getElementBy(settingsButton_checkPage_AdminField));
        WebElement element = getElement(settingsButton_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openAdministrators() {
        waitForElementVisible(getElementBy(administrators_AdminField));
        getElement(administrators_AdminField).click();
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openAdministrators_checkPage() {
        waitForElementClickable(getElementBy(administrators_checkPage_AdminField));
        WebElement element = getElement(administrators_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openGlobalSettings() {
        waitForElementVisible(getElementBy(globalSettings_AdminField));
        getElement(globalSettings_AdminField).click();
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openGlobalSettings_checkPage() {
        waitForElementClickable(getElementBy(globalSettings_checkPage_AdminField));
        WebElement element = getElement(globalSettings_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openAffiliates() {
        waitForElementVisible(getElementBy(affiliates_AdminField));
        getElement(affiliates_AdminField).click();
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openAffiliates_checkPage() {
        waitForElementClickable(getElementBy(affiliates_checkPage_AdminField));
        WebElement element = getElement(affiliates_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openSettingReminders() {
        waitForElementVisible(getElementBy(settingReminders_AdminField));
        getElement(settingReminders_AdminField).click();
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openSettingReminders_checkPage() {
        waitForElementClickable(getElementBy(settingReminders_checkPage_AdminField));
        WebElement element = getElement(settingReminders_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openAreas() {
        waitForElementVisible(getElementBy(areas_AdminField));
        getElement(areas_AdminField).click();
        return this;
    }

    public Page_9_AGIS2_Settings_Tab openAreas_checkPage() {
        waitForElementClickable(getElementBy(areas_checkPage_AdminField));
        WebElement element = getElement(areas_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
