package aventus.webMd.pages.applicationTabPages;

import aventus.agis2.pages.AbstractPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class ApplicationsTabPage extends AbstractPage {

    String applicationsButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[1]/a";
    String openApplications_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[1]/ul/li[1]/a";

    String allApplicationsButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[1]/ul/li[1]/a";
    String allApplications_CheckPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[2]";

    String newApplicationsButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[1]/ul/li[2]/a";
    String newApplications_CheckPage_AdminField = "/html/body/div[1]/div/section[2]/div[2]/div/div/div[1]/table/tbody/tr[1]/td[1]/div/a[1]";

    String onCustomerSignatureButton_AdminField = "/html/body/div[1]/aside/div/section/ul/li[1]/ul/li[3]/a";
    String onCustomerSignature_CheckPage_AdminField = "/html/body/div[1]/div/section[1]/div/nav/div/div";

    String confirmedButton_AdminField = "/html/body/div[1]/aside/div/section/ul/li[1]/ul/li[4]/a";
    String confirmed_CheckPage_AdminField = "/html/body/div[1]/div/section[1]/div/nav/div/div";




    public ApplicationsTabPage openApplications() {
        waitForElementVisible(getElementBy(applicationsButton_AdminField));
        getElement(applicationsButton_AdminField).click();
        return this;
    }

    public ApplicationsTabPage openApplications_checkPage() {
        waitForElementClickable(getElementBy(openApplications_checkPage_AdminField));
        WebElement element = getElement(openApplications_checkPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }



    public ApplicationsTabPage openAllApplications() {
        waitForElementVisible(getElementBy(allApplicationsButton_AdminField));
        getElement(allApplicationsButton_AdminField).click();
        return this;
    }

    public ApplicationsTabPage allAplications_CheckPage() {
        waitForElementClickable(getElementBy(allApplications_CheckPage_AdminField));
        WebElement element = getElement(allApplications_CheckPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }


    public ApplicationsTabPage newApplications() {
        waitForElementVisible(getElementBy(newApplicationsButton_AdminField));
        getElement(newApplicationsButton_AdminField).click();
        return this;
    }

    public ApplicationsTabPage newApplications_CheckPage() {
        waitForElementClickable(getElementBy(newApplications_CheckPage_AdminField));
        WebElement element = getElement(newApplications_CheckPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public ApplicationsTabPage onCustomerSignature() {
        waitForElementVisible(getElementBy(onCustomerSignatureButton_AdminField));
        getElement(onCustomerSignatureButton_AdminField).click();
        return this;
    }

    public ApplicationsTabPage onCustomerSignature_CheckPage() {
        waitForElementClickable(getElementBy(onCustomerSignature_CheckPage_AdminField));
        WebElement element = getElement(onCustomerSignature_CheckPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

    public ApplicationsTabPage click_confirmedButton() {
        waitForElementVisible(getElementBy(confirmedButton_AdminField));
        getElement(confirmedButton_AdminField).click();
        return this;
    }

    public ApplicationsTabPage confirmed_CheckPage() {
        waitForElementClickable(getElementBy(confirmed_CheckPage_AdminField));
        WebElement element = getElement(confirmed_CheckPage_AdminField);
        Assert.assertEquals(true, element.isDisplayed());
        return this;
    }

}
