package aventus.webMd.pages.makingDecisionsTabPages;

import aventus.agis2.pages.AbstractPage;

public class Page_6_AGIS2_MakingDecisions_Tab extends AbstractPage {

    String makingDecisionsTabButton_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[5]/a/span[1]";
    String makingDecisions_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[5]/ul/li[1]/a";

    String rules_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[5]/ul/li[1]/a";
    String rules_checkPage_AdminField = "/html/body/div[1]/header/nav/div[1]/div/ol/li[2]/span";

    String ruleGroups_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[5]/ul/li[2]/a";
    String ruleGroups_checkPage_AdminField = "/html/body/div[1]/aside/div[1]/section/ul/li[5]/ul/li[2]/a";

    public Page_6_AGIS2_MakingDecisions_Tab openMakingDecisionsTab() {
        waitForElementVisible(getElementBy(makingDecisionsTabButton_AdminField));
        getElement(makingDecisionsTabButton_AdminField).click();
        return this;
    }

    public Page_6_AGIS2_MakingDecisions_Tab makingDecisionsTab_checkPage() {
        waitForElementVisible(getElementBy(makingDecisions_checkPage_AdminField));
        getElement(makingDecisions_checkPage_AdminField).click();
        return this;
    }

    public Page_6_AGIS2_MakingDecisions_Tab openRulesTab() {
        waitForElementVisible(getElementBy(rules_AdminField));
        getElement(rules_AdminField).click();
        return this;
    }

    public Page_6_AGIS2_MakingDecisions_Tab rulesTab_checkPage() {
        waitForElementVisible(getElementBy(rules_checkPage_AdminField));
        getElement(rules_checkPage_AdminField).click();
        return this;
    }

    public Page_6_AGIS2_MakingDecisions_Tab openRuleGroups() {
        waitForElementVisible(getElementBy(ruleGroups_AdminField));
        getElement(ruleGroups_AdminField).click();
        return this;
    }

    public Page_6_AGIS2_MakingDecisions_Tab rulesGroupsTab_checkPage() {
        waitForElementVisible(getElementBy(ruleGroups_checkPage_AdminField));
        getElement(ruleGroups_checkPage_AdminField).click();
        return this;
    }

}
