package aventus.webMd.helpers.translationsTabHelpers;

import aventus.webMd.pages.translationsTabPages.Page_12_AGIS2_Translations_Tab;

public class Helper_12_AGIS2_Translations_Tab {

    aventus.webMd.pages.translationsTabPages.Page_12_AGIS2_Translations_Tab Page_12_AGIS2_Translations_Tab = new Page_12_AGIS2_Translations_Tab();

    public void checkTranslations() {
        Page_12_AGIS2_Translations_Tab.openTranslationsButton()
                .openSmsTemplates()
                .smsTemplates_checkPage()
                .openEmailTemplates()
                .emailTemplates_checkPage()
                .openSitePages()
                .sitePages_checkPage()
                .openFAQ()
                .faq_checkPage();
    }
}