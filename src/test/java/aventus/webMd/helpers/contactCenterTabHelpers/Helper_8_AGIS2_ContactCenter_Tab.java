package aventus.webMd.helpers.contactCenterTabHelpers;

import aventus.webMd.pages.contactCenterTabPages.Page_8_AGIS2_ContactCenter_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_8_AGIS2_ContactCenter_Tab extends AbsractHelper {

    aventus.webMd.pages.contactCenterTabPages.Page_8_AGIS2_ContactCenter_Tab Page_8_AGIS2_ContactCenter_Tab = new Page_8_AGIS2_ContactCenter_Tab();

    public void checkContactCenter() {
        Page_8_AGIS2_ContactCenter_Tab.openContactCenterTab()
                .openContactCenterTab_checkPage()
                .openContactCenter()
                .openContactCenter_checkPage()
                .openHistory()
                .openHistory_checkPage()
                .openSettings()
                .openSettings_checkPage()
                .openOnlineReport()
                .openOnlineReport_checkPage()
                .openContactsReport()
                .openContactsReport_checkPage();
    }
}
