package aventus.webMd.helpers.clientsTabHelpers;

import aventus.webMd.pages.clientsTabPages.Page_7_AGIS2_Clients_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_7_AGIS2_Clients_Tab extends AbsractHelper {

    aventus.webMd.pages.clientsTabPages.Page_7_AGIS2_Clients_Tab Page_7_AGIS2_Clients_Tab = new Page_7_AGIS2_Clients_Tab();

    public void checkClients() {
        Page_7_AGIS2_Clients_Tab.openClients()
                .openClients_checkPage()
                .openAllClients()
                .openAllClients_checkPage()
                .openRepeatedRegistrations()
                .openRepeatedRegistrations_checkPage()
                .openNoApplications()
                .openNoApplications_checkPage()
                .openBlackList()
                .openBlackList_checkPage();
    }

}
