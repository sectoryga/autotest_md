package aventus.webMd.helpers.reportTabHelpers;

import aventus.webMd.pages.reportTabPages.Page_11_AGIS2_Report_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_11_AGIS2_Report_Tab extends AbsractHelper {

    aventus.webMd.pages.reportTabPages.Page_11_AGIS2_Report_Tab Page_11_AGIS2_Report_Tab = new Page_11_AGIS2_Report_Tab();

    public void checkReportTab() {
        Page_11_AGIS2_Report_Tab.openReportTab()
                .reportTab_checkPage()
                .openAffiliateUsers()
                .affiliateUsers_checkPage()
                .openAffiliateСredits()
                .affiliateСredits_checkPage()
                .openAffiliateRequests()
                .affiliateRequests_checkPage()
                .openSummaryReport()
                .summaryReport_checkPage()
                .openCashflowReport()
                .cashflowReport_checkPage()
                .openIncomeReport()
                .incomeReport_checkPage()
                .openReportProcessor()
                .reportProcessor_checkPage()
                .openUniversalReport()
                .universalReport_checkPage()
                .openSmsReport()
                .smsReport_checkPage();
    }

}
