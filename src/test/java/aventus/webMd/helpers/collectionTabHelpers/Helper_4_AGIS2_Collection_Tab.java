package aventus.webMd.helpers.collectionTabHelpers;

import aventus.webMd.pages.collectionTabPages.Page_4_AGIS2_Collection_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_4_AGIS2_Collection_Tab extends AbsractHelper {

    aventus.webMd.pages.collectionTabPages.Page_4_AGIS2_Collection_Tab Page_4_AGIS2_Collection_Tab = new Page_4_AGIS2_Collection_Tab();

    public void checkCollectiomTab() {

        Page_4_AGIS2_Collection_Tab.openCollectionTab()
                .openCollection()
                .collection_checkPage()
                .openCollectionInhouse()
                .collectionInhouse_checkPage()
                .openCollectionOutsource()
                .collectionOutsource_checkPage()
                .openCollectionLegal()
                .collectionLegal_checkPage()
                .openCollectionIncome()
                .collectionIncome_checkPage()
                .openCollectionSetCollectorsList()
                .collectionSetCollectorsList_checkPage();

    }

}
