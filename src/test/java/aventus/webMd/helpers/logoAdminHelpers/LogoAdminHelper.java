package aventus.webMd.helpers.logoAdminHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.logoAdminPages.LogoAdminPage;

public class LogoAdminHelper extends AbsractHelper {

    LogoAdminPage adminLoginAGIS2 = new LogoAdminPage();

    public LogoAdminHelper getAdminLoginAGIS2() {
        adminLoginAGIS2.openMainPageAGIS2()
                .setAdminUserName()
                .setAdminUserPassword()
                .clickLoginButtonAGIS2()
                .checkLogo();
        return this;
    }
}
