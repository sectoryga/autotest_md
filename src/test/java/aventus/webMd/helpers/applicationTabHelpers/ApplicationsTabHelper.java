package aventus.webMd.helpers.applicationTabHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.applicationTabPages.ApplicationsTabPage;

public class ApplicationsTabHelper extends AbsractHelper {

    ApplicationsTabPage AGIS2_Applications_Tab = new ApplicationsTabPage();

    public void checkApplicationsTab() {
        AGIS2_Applications_Tab.openApplications()
                .openApplications_checkPage()
                .openAllApplications()
                .allAplications_CheckPage()
                .newApplications()
                .newApplications_CheckPage()
                .onCustomerSignature()
                .onCustomerSignature_CheckPage()
                .click_confirmedButton()
                .confirmed_CheckPage();

    }

}
