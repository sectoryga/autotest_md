package aventus.webMd.helpers.makingDecisionsTabHelpers;


import aventus.webMd.pages.makingDecisionsTabPages.Page_6_AGIS2_MakingDecisions_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_6_AGIS2_MakingDecisions_Tab extends AbsractHelper {

    aventus.webMd.pages.makingDecisionsTabPages.Page_6_AGIS2_MakingDecisions_Tab Page_6_AGIS2_MakingDecisions_Tab = new Page_6_AGIS2_MakingDecisions_Tab();

    public void checkMakingDecisions() {
        Page_6_AGIS2_MakingDecisions_Tab.openMakingDecisionsTab()
                .makingDecisionsTab_checkPage()
                .openRulesTab()
                .rulesTab_checkPage()
                .openRuleGroups()
                .rulesGroupsTab_checkPage();
    }

}
