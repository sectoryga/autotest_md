package aventus.webMd.helpers.creditCardsTabHelpers;

import aventus.webMd.pages.creditCardsTabPages.Page_5_AGIS2_CreditCards_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_5_AGIS2_CreditCards_Tab extends AbsractHelper {

    Page_5_AGIS2_CreditCards_Tab Page_5_AGIS2_СreditCards_Tab = new Page_5_AGIS2_CreditCards_Tab();

    public void checkСreditCardsTab() {
        Page_5_AGIS2_СreditCards_Tab.openCreditCardsTab()
                .creditCardsTab_checkPage()
                .openCardTransactions1()
                .cardTransactions1_checkPage()
                .openCardVerification()
                .cardVerification_checkPage()
                .openCardTransactions2()
                .cardTransactions2_checkPage();
    }

}
