package aventus.webMd.helpers.loanTabHelpers;

import aventus.webMd.pages.loanTabPages.LoansTabPage;
import aventus.webMd.helpers.AbsractHelper;

public class LoansTabHelper extends AbsractHelper {

    LoansTabPage AGIS2_Loans_Tab = new LoansTabPage();

    public void checkLoansTab() {
        AGIS2_Loans_Tab.openLoans()
                .loans_checkPage()
                .openInProcessingTab()
                .inProcessingTab_checkPage()
                .openProcessingErrorTab()
                .processingErrorTab_checkPage()
                .openActiveTab()
                .activeTab_checkPage()
                .openExtendedTab()
                .extendedTab_checkPage()
                .openOverdueTab()
                .overdueTab_checkPage()
                .openRepaidTab()
                .repaidTab_checkPage()
                .openSoldRepaidTab()
                .soldTab_checkPage();
    }
}
