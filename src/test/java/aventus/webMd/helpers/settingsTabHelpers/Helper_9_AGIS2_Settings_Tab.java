package aventus.webMd.helpers.settingsTabHelpers;

import aventus.webMd.pages.settingsTabPages.Page_9_AGIS2_Settings_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_9_AGIS2_Settings_Tab extends AbsractHelper {

    aventus.webMd.pages.settingsTabPages.Page_9_AGIS2_Settings_Tab Page_9_AGIS2_Settings_Tab = new Page_9_AGIS2_Settings_Tab();


    public void checkSettingsTab() {
        Page_9_AGIS2_Settings_Tab.openSettingsTab()
                .openSettingsTab_checkPage()
                .openAdministrators()
                .openAdministrators_checkPage()
                .openGlobalSettings()
                .openGlobalSettings_checkPage()
                .openAffiliates()
                .openAffiliates_checkPage()
                .openSettingReminders()
                .openSettingReminders_checkPage()
                .openAreas()
                .openAreas_checkPage();

    }
}
