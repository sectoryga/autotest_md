package aventus.webMd.helpers.restTabHelpers;

import aventus.webMd.pages.restTabPages.Page_10_AGIS2_Rest_Tab;
import aventus.webMd.helpers.AbsractHelper;

public class Helper_10_AGIS2_Rest_Tab extends AbsractHelper {

    aventus.webMd.pages.restTabPages.Page_10_AGIS2_Rest_Tab Page_10_AGIS2_Rest_Tab = new Page_10_AGIS2_Rest_Tab();

    public void checkRestTab() {
        Page_10_AGIS2_Rest_Tab.openRestTab()
                .restTab_checkPage()
                .openDiscounts()
                .discounts_checkPage()
                .openPriceLists()
                .priceLists_checkPage()
                .openCommentStyle()
                .commentStyle_checkPage()
                .openSmsSenders()
                .smsSenders_checkPage()
                .openStatements()
                .statements_checkPage()
                .openDocumentTemplates()
                .documentTemplates_checkPage()
                .openAutoAssignmentOfVerifiers()
                .autoAssignmentOfVerifiers_checkPage();

    }

}
