package aventus.webMd.helpers.frontRegHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.frontRegPages.RegistrationStep4Page;

public class RegistrationStep4Helper extends AbsractHelper {

    RegistrationStep4Page registrationStep4 = new RegistrationStep4Page();

    public void getCredit7_Step4() {
        registrationStep4.setUserMaritalStatus()
                .setUserEducationStatus()
                .setUserActivityType()
                .setUserOrganizationName()
                .setUserMonthlyIncome()
                .clickContinueButton();
    }
}
