package aventus.webMd.helpers.frontRegHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.logoAdminPages.LogoAdminPage;
import aventus.webMd.pages.frontRegPages.RegistrationStep6Page;

import java.awt.*;

public class RegistrationStep6Helper extends AbsractHelper {

    RegistrationStep6Page registrationStep6 = new RegistrationStep6Page();
    LogoAdminPage adminLoginAGIS2 = new LogoAdminPage();

    public void getCredit7_Step6() throws AWTException, InterruptedException {
        registrationStep6.openNewTabInChrome();
        adminLoginAGIS2.openMainPageAGIS2()
                .setAdminUserName()
                .setAdminUserPassword()
                .clickLoginButtonAGIS2();
        registrationStep6.getSMS_UserRegistrationConfirmation();
        Thread.sleep(2000);
        registrationStep6.newTabs(driver);
        registrationStep6.pasteSMS();
        registrationStep6.clickSendLoanButton();
        Thread.sleep(2000);
    }
}
