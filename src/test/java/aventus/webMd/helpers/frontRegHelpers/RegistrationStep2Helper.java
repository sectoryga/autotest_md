
package aventus.webMd.helpers.frontRegHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.frontRegPages.RegistrationStep2Page;

public class RegistrationStep2Helper extends AbsractHelper {

    RegistrationStep2Page registration2stStep = new RegistrationStep2Page();

    public void getCredit7_2() throws InterruptedException {
        registration2stStep.setUserSurname()
                .setUserName()
                .setUserPhone()
                .setUserPassword()
                .setConsentCheckBox1()
                .setConsentCheckBox2()
                .clickContinueButton()
                .clickContinueButton();
    }
}

