package aventus.webMd.helpers.frontRegHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.frontRegPages.RegistrationStep5Page;

public class RegistrationStep5Helper extends AbsractHelper {

    RegistrationStep5Page registrationStep5 = new RegistrationStep5Page();

    public void getCredit7_Step5() throws InterruptedException {
        registrationStep5.setUserDocument1()
                .setUserDocument2()
                .clickContinueButton();
    }

}
