package aventus.webMd.helpers.frontRegHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.frontRegPages.RegistrationStep1Page;

public class RegistrationStep1Helper extends AbsractHelper {

    RegistrationStep1Page loginPageCredit7 = new RegistrationStep1Page();

    public RegistrationStep1Helper getCredit7() {
                loginPageCredit7.openMainPageCredit7()
                .setPriceCredit7()
                .clickGetMoneyButton();
        return this;
    }
}
