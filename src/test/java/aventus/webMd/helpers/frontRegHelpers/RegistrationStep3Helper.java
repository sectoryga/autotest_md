package aventus.webMd.helpers.frontRegHelpers;

import aventus.webMd.helpers.AbsractHelper;
import aventus.webMd.pages.frontRegPages.RegistrationStep3Page;

public class RegistrationStep3Helper extends AbsractHelper {

    RegistrationStep3Page registration3rdStep = new RegistrationStep3Page();

    public void getCredit7_3() {
        registration3rdStep.setUserIDNP()
                .setUserDateOfBirth()
                .setUserGender()
                .setUserEmail()
                .setUserIBAN()
                .clickContinueButton()
                .clickContinueButton();
    }
}
