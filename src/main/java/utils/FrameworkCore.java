package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;


public class FrameworkCore {
    private static WebDriver webDriver;
    private static String browser = IOUtils.loadGenericProperties("browser", "configuration");
    public static String Credit7Url = IOUtils.loadGenericProperties("Credit7Url", "configuration");
    public static String Credit365_URL = IOUtils.loadGenericProperties("Credit365_URL", "configuration");
    public static String AGIS2_URL = IOUtils.loadGenericProperties("AGIS2_URL", "configuration");
    public static String adminUserName = IOUtils.loadGenericProperties("admin_UserName", "configuration");
    public static String adminUserPassword = IOUtils.loadGenericProperties("admin_UserPassword", "configuration");
    public static String AGIS2_allUsersURL = IOUtils.loadGenericProperties("AGIS2_allUsersURL", "configuration");
    public static String AGIS2_AllApplicationsAndLoans = IOUtils.loadGenericProperties("AGIS2_AllApplicationsAndLoans", "configuration");


    public static WebDriver getInstance() {
        if (browser.equals(BrowserConstants.CHROME)) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--window-size=1920,1080");
            options.addArguments("disable-dev-shm-usage");
            options.addArguments("--headless");    //start without chrome view
            webDriver = new ChromeDriver(options);
        } else {
            if (browser.equals(BrowserConstants.FIREFOX)) {
                webDriver = new FirefoxDriver();
            } else {
                throw new IllegalArgumentException("Browser value from property file is incorrect!");
            }
        }

        return webDriver;
    }
}
