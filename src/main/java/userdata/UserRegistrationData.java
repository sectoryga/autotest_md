package userdata;

public class UserRegistrationData {
    private String userIDNP;
    private String userLastName;
    private String userPhone;
    private String userBankAccountNumber;


    public String getUserRandomTelephoneNumber() {
        return userPhone = "794" + (11111+ (int) (Math.random() * (100000 - 11111)));
    }

    public String getUserLastName() {

        return userLastName = "Ivanov";
    }

    public String getUserIDNP() {
        IDNPGenerator idnpGenerator = new IDNPGenerator();
        return userIDNP = idnpGenerator.idnpVerifivation();
    }

    public String getUserBankAccountNumber() {
        return userBankAccountNumber = "MD24AG000225100013104168";
    }
}
